package com.opnd.dl.reports;

import com.opnd.dl.domain.wrapper.InventoryWrapper;
import com.opnd.dl.domain.wrapper.PaymentsReportWrapper;
import com.opnd.dl.domain.wrapper.SalesReportWrapper;
import com.opnd.dl.mStuff.LogUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class CSV {

    private String DELIMETER = ",";
    private String NEWLINE = "\n";
    private String FILETYPE = ".csv";
    private String reportsLocation = "reports/csv-files";

    public CSV() {
        File reportFolder = new File(reportsLocation);
        boolean mkdirs = true;
        if (!reportFolder.exists()) {
            mkdirs = reportFolder.mkdirs();
        }
        if(!mkdirs) System.out.println("Something happend I couldn't create the directory");
    }

    public void paymentReport(List<PaymentsReportWrapper> list){
        File paymentfolder = new File(reportsLocation+"/payments");
        if(!paymentfolder.exists()) paymentfolder.mkdirs();

        try {
            String filename = "reports/csv-files/payments/"+"payment-"+
                    LocalDateTime.now().toString().replaceAll(":","-").replace("T","_");
            filename = filename.substring(0, filename.length() - 4)+".csv";
            LogUtil.log(filename);
            FileWriter fileWriter = new FileWriter(filename);
            fileWriter.append("Date,Time,Location,Amount, PaidBy");
            DecimalFormat df = new DecimalFormat("#######.##");
            for (PaymentsReportWrapper paymentsReportWrapper : list) {
                fileWriter.append(NEWLINE);
                fileWriter.append(DateFormat.getDateInstance().format(paymentsReportWrapper.getDate()));
                fileWriter.append(DELIMETER);
                fileWriter.append(DateFormat.getTimeInstance().format(paymentsReportWrapper.getDate()));
                fileWriter.append(DELIMETER);
                fileWriter.append(paymentsReportWrapper.getLocation().getDescription());
                fileWriter.append(DELIMETER);
                fileWriter.append(df.format(paymentsReportWrapper.getAmount()));
                fileWriter.append(DELIMETER);
                fileWriter.append(paymentsReportWrapper.getPaidBy().getFullname());
                fileWriter.append(DELIMETER);
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saleReport(List<SalesReportWrapper> list){
        File salesfolder = new File(reportsLocation+"/sales");
        if(!salesfolder.exists()) salesfolder.mkdirs();

        try {
            String filename = "reports/csv-files/sales/"+"sales-"+
                    LocalDateTime.now().toString().replaceAll(":","-").replace("T","_");
            filename = filename.substring(0, filename.length() - 4)+".csv";
            LogUtil.log(filename);
            FileWriter fileWriter = new FileWriter(filename);
            fileWriter.append("Date,Time,Location,Product, Quantity, Unit Price, Amount, Sales Person");
            DecimalFormat df = new DecimalFormat("#######.##");
            for (SalesReportWrapper salesReportWrapper : list) {
                fileWriter.append(NEWLINE);
                fileWriter.append(DateFormat.getDateInstance().format(salesReportWrapper.getDate()));
                fileWriter.append(DELIMETER);
                fileWriter.append(DateFormat.getTimeInstance().format(salesReportWrapper.getDate()));
                fileWriter.append(DELIMETER);
                fileWriter.append(salesReportWrapper.getLocation().getDescription());
                fileWriter.append(DELIMETER);
                fileWriter.append(new DecimalFormat("#,###,###.##").format(salesReportWrapper.getQuantity()));
                fileWriter.append(DELIMETER);
                fileWriter.append(new DecimalFormat("#,###,###.##").format(salesReportWrapper.getPrice()));
                fileWriter.append(DELIMETER);
                fileWriter.append(new DecimalFormat("#,###,###.##").format(salesReportWrapper.getAmount()));
                fileWriter.append(DELIMETER);
                fileWriter.append(salesReportWrapper.getSalesperson().getFullname());
                fileWriter.append(DELIMETER);
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void inventoryReport(List<InventoryWrapper> list){
        File inventoryLocation = new File(reportsLocation+"/inventory");
        if(!inventoryLocation.exists()) inventoryLocation.mkdirs();

        try {
            String filename = "reports/csv-files/inventory/"+"inventory-"+
                    LocalDateTime.now().toString().replaceAll(":","-").replace("T","_");
            filename = filename.substring(0, filename.length() - 4)+".csv";
            LogUtil.log(filename);
            FileWriter fileWriter = new FileWriter(filename);
            fileWriter.append("Date,Location,Product, Category, Quantity, Unit Price, Amount");
            DecimalFormat df = new DecimalFormat("#######.##");
            for (InventoryWrapper item : list) {
                fileWriter.append(NEWLINE);
                fileWriter.append(DateFormat.getDateInstance().format(item.getDate()).replace(",",""));
                fileWriter.append(DELIMETER);
                fileWriter.append(item.getLocation().getDescription());
                fileWriter.append(DELIMETER);
                fileWriter.append(item.getProduct().getDescription());
                fileWriter.append(DELIMETER);
                fileWriter.append(item.getCategory().getDescription());
                fileWriter.append(DELIMETER);
                fileWriter.append(df.format(item.getQuantity()));
                fileWriter.append(DELIMETER);
                fileWriter.append(df.format(item.getUnitprice()));
                fileWriter.append(DELIMETER);
                fileWriter.append(df.format(item.getAmount()));
                fileWriter.append(DELIMETER);
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
