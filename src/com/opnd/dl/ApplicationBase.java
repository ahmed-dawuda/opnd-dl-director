package com.opnd.dl;

import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.reports.CSV;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class ApplicationBase extends Application {

    private double xOffset = 0;
    private double yOffset = 0;

    public static void main(String[] args) {
//        CSV csv = new CSV();
            launch(args);
        }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Persistor.init("SQLite");

        Parent root = FXMLLoader.load(getClass().getResource("/com/opnd/dl/view/director/login.fxml"));
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        primaryStage.setTitle("Desert Lion");
        primaryStage.setScene(new Scene(root));
//        primaryStage.getScene().getStylesheets().add(getClass().getResource("/resources/css/stylesheet.css").toExternalForm());
//        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.setOnCloseRequest(event -> Persistor.close());
//        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();

    }
}
