package com.opnd.dl.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Role;
import com.opnd.dl.model.EditUtil;
import com.opnd.dl.model.LocationUtil;
import com.opnd.dl.model.RoleUtil;
import com.opnd.dl.model.UserUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;
import java.util.UUID;

public class AddWorkerController {
    @FXML private JFXTextField firstname;
    @FXML private JFXTextField surname;
    @FXML private JFXTextField middlename;
    @FXML private JFXComboBox<Role> role;
    @FXML private JFXTextField password;
    @FXML private JFXButton addBtn;
    @FXML private Label message;
    @FXML private JFXComboBox<Location> loc;

    private RoleUtil roleUtil;
    private UserUtil userUtil;
    private LocationUtil locationUtil;

    private String fname;
    private String sname;
    private String mname;
    private String r = "0";
    private String pwd;

    public void initialize(){
        locationUtil = new LocationUtil();
        this.addBtn.setDisable(true);
        userUtil = new UserUtil();
        role.setConverter(new StringConverter<Role>() {
            @Override
            public String toString(Role object) {
                return object.getDescription();
            }
            @Override
            public Role fromString(String string) {
                return null;
            }
        });

        role.setCellFactory(param -> new ListCell<Role>(){
            @Override
            protected void updateItem(Role item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        loc.setConverter(new StringConverter<Location>() {
            @Override
            public String toString(Location object) {
                return object.getDescription();
            }

            @Override
            public Location fromString(String string) {
                return null;
            }
        });

        loc.setCellFactory(param -> new ListCell<Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        loc.setItems(FXCollections.observableArrayList(locationUtil.allLocations()));

        this.roleUtil = new RoleUtil();
        System.out.println(roleUtil.allRoles());
        role.setItems(FXCollections.observableArrayList(roleUtil.allRoles()));

        password.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()) this.addBtn.setDisable(true);
            else this.addBtn.setDisable(false);
        });

        password.setEditable(false);
    }

    public void add(){
        fname = firstname.getText();
        sname = surname.getText();
        mname = middlename.getText();
        if(!role.getSelectionModel().isEmpty()){
            r = role.getSelectionModel().getSelectedItem().getId();
        }else{
            this.message.setText("Please select a role for this worker");
            return;
        }
        pwd = password.getText();
        System.out.println(fname+" "+mname+" "+sname+" "+r+" "+pwd);
        if(     !fname.isEmpty() &&
                !mname.isEmpty() &&
                !sname.isEmpty() &&
                !pwd.isEmpty() &&
                !role.getSelectionModel().isEmpty() &&
                !loc.getSelectionModel().isEmpty()
                ){

            userUtil.createSubject(fname,mname,sname, r,pwd, loc.getSelectionModel().getSelectedItem().getId());
            this.message.setText("Worker has been added");
            cancel();
        } else
            this.message.setText("Please specify values for all fields");
    }

    public void generatePassword(){
        this.password.setText( UUID.randomUUID().toString().substring(0,4) + firstname.getText());
    }

    public void cancel(){
        password.clear();
        firstname.clear();
        surname.clear();
        middlename.clear();
        role.getSelectionModel().clearSelection();
    }
}
