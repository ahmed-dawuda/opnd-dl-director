package com.opnd.dl.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;

import java.util.concurrent.TimeUnit;

public class LoginController {

//    @FXML private ProgressIndicator progress;
    @FXML private TextField username;
    @FXML private PasswordField password;
    @FXML private Label message;
    @FXML private Label loading;

    private LoginFX loginFX = new LoginFX();

    @FXML
    public void initialize() {
//        progress.setVisible(false);
        loginFX.username.bindBidirectional(username.textProperty());
        loginFX.password.bindBidirectional(password.textProperty());
        loginFX.message.bindBidirectional(message.textProperty());
        loginFX.loading.bindBidirectional(loading.textProperty());
        message.setVisible(false);
        loading.setVisible(false);
        LogUtil.log("Got here");
    }

    @FXML
//    private void login() throws InterruptedException {
//        if (loginFX.getUsername().trim().isEmpty()) {
//            LogUtil.log("Please enter the username");
//            loginFX.message.set("Please enter the username");
//            message.setVisible(true);
//            return;
//        }
//
//        if (loginFX.getPassword().isEmpty()) {
//            LogUtil.log("Please enter the password");
//            loginFX.message.set("Please enter the password");
//            message.setVisible(true);
//            return;
//        }
//        loginFX.loading.set("loading...");
//        loading.setVisible(true);
////        Thread.sleep(2000);
////        progress.setVisible(true);
//        UserUtil user = new UserUtil();
//        Subject subject = user.login(loginFX.getUsername(), loginFX.getPassword());
//        if (subject == null) {
////            progress.setVisible(false);
//            loginFX.message.set("Could not log in. \nPlease check your credentials");
//
//            loading.setVisible(false);
//            LogUtil.log("Could not log in. Please check your credentials");
//            return;
//        }else{
//            GlobalValues.setCurrentSubject(subject);
//        }
//
////        progress.setVisible(false);
//        LogUtil.log("Welcome " + subject.getFirstname());
//        ViewHelper viewHelper = new ViewHelper();
//        viewHelper.switchScene("home",true,username);
//    }

    private void login() throws InterruptedException {
        if (loginFX.getUsername().trim().isEmpty()) {
            LogUtil.log("Please enter the username");
            loginFX.message.set("Please enter the username");
            message.setVisible(true);
            return;
        }

        if (loginFX.getPassword().isEmpty()) {
            LogUtil.log("Please enter the password");
            loginFX.message.set("Please enter the password");
            message.setVisible(true);
            return;
        }
        loginFX.loading.set("loading...");
        loading.setVisible(true);
//        Thread.sleep(2000);
//        progress.setVisible(true);
        new Thread(() -> {
            UserUtil user = new UserUtil();
            Subject subject = user.login(loginFX.getUsername(), loginFX.getPassword());
            if (subject == null) {
//            progress.setVisible(false);
                Platform.runLater(() -> {
                    loginFX.message.set("Could not log in. \nPlease check your credentials");
                    loading.setVisible(false);
                });
                LogUtil.log("Could not log in. Please check your credentials");
                return;
            }else{
                GlobalValues.setCurrentSubject(subject);
                new Thread(() -> {
                    ProductUtil productUtil = new ProductUtil();
                    PaymentUtil paymentUtil = new PaymentUtil();
                    TransferUtil transferUtil = new TransferUtil();
                    RequisitionUtil requisitionUtil = new RequisitionUtil();
                    CategoryUtil categoryUtil = new CategoryUtil();
                    InventoryUtil inventoryUtil = new InventoryUtil();
                    LocationUtil locationUtil = new LocationUtil();
                    RoleUtil roleUtil = new RoleUtil();
                    SaleUtil saleUtil = new SaleUtil();
                    StockUtil stockUtil = new StockUtil();
                    UserUtil userUtil = new UserUtil();

                    GlobalValues.setRunning(true);
                    while(GlobalValues.isRunning()){
                        try {
//                            userUtil.sync();
                            roleUtil.sync();
                            locationUtil.sync();
                            inventoryUtil.sync();
                            productUtil.postUnsynced();
                            requisitionUtil.sync();
                            transferUtil.sync();
                            categoryUtil.postUnsynced();

                            productUtil.sync();

                            locationUtil.postUnsynced();
                            userUtil.postUnsynced();
                            paymentUtil.sync();
                            roleUtil.postUnsynced();
                            saleUtil.sync();
//                            stockUtil.sync();

//                            updating
                            requisitionUtil.putUpdated();
                            paymentUtil.sync();
                            inventoryUtil.updateInventoryDB();
                            productUtil.updateProductDB();

                            TimeUnit.SECONDS.sleep(10);
                        } catch (UnirestException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
//        progress.setVisible(false);
            Platform.runLater(() -> {
                LogUtil.log("Welcome " + subject.getFirstname());
                ViewHelper viewHelper = new ViewHelper();
                viewHelper.switchScene("home",true, username);
            });
        }).start();
    }

    @FXML
    private void cancel() {
        username.clear();
        password.clear();
        message.setVisible(false);
        loading.setVisible(false);

        username.requestFocus();
    }

    @FXML
    private void close() {
        Platform.exit();
    }

    private class LoginFX {
        private StringProperty username = new SimpleStringProperty();
        private StringProperty password = new SimpleStringProperty();
        private StringProperty message = new SimpleStringProperty();
        private StringProperty loading = new SimpleStringProperty();

        public String getLoading() {
            return loading.get();
        }

        public StringProperty loadingProperty() {
            return loading;
        }

        public void setLoading(String loading) {
            this.loading.set(loading);
        }

        public String getMessage() {
            return message.get();
        }

        public StringProperty messageProperty() {
            return message;
        }

        public void setMessage(String message) {
            this.message.set(message);
        }

        public String getUsername() {
            return username.get();
        }

        public StringProperty usernameProperty() {
            return username;
        }

        public void setUsername(String username) {
            this.username.set(username);
        }

        public String getPassword() {
            return password.get();
        }

        public StringProperty passwordProperty() {
            return password;
        }

        public void setPassword(String password) {
            this.password.set(password);
        }
    }
}
