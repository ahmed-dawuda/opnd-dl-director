package com.opnd.dl.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.Product;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.CategoryUtil;
import com.opnd.dl.model.EditUtil;
import com.opnd.dl.model.ProductUtil;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;

public class CreateProductController {

    @FXML private JFXTextField description;
    @FXML private JFXComboBox<Category> category;
    @FXML private JFXTextField price;

    @FXML private Label message;
    @FXML private JFXButton addProduct;

    private double priceValue = 0;
    private CategoryUtil categoryUtil;

    @FXML
    public void initialize() {

        this.categoryUtil = new CategoryUtil();

//        message.setVisible(false);
        price.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;

            if (!newValue.matches("^[0-9]*\\.?[0-9]*$")) price.setText(oldValue);
            else priceValue = Double.parseDouble(newValue);
        });

        this.category.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getDescription();
            }

            @Override
            public Category fromString(String string) {
                return null;
            }
        });

        this.category.setCellFactory(param -> new ListCell<Category>(){
            @Override
            protected void updateItem(Category item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });


        category.setItems(FXCollections.observableArrayList(categoryUtil.allCategories()));

        if(EditUtil.isAvailable()){
            addProduct.setText("Edit Product");
            System.out.println("Product is set to edit");
            this.description.setText(EditUtil.getProduct().getDescription());
            this.price.setText(EditUtil.getProduct().getPrice() + "");
            this.category.getSelectionModel()
                    .select((Category) Persistor.find(Category.class,EditUtil.getProduct().getCategoryId()));
        }
    }

    @FXML
    private void createProduct() {
//        System.out.println(priceValue);
        String desc = description.getText();
        Category cat = category.getSelectionModel().getSelectedItem();
        if (desc.isEmpty()) {
            LogUtil.log("You have not entered the description");
            message.setText("You have not entered the description");
            message.setVisible(true);
            return;
        }

        if (priceValue == 0) {
            LogUtil.log("You have not entered the price");
            message.setText("You have not entered the price");
            message.setVisible(true);
            return;
        }

        if (cat == null) {
            LogUtil.log("There is no category");
            message.setText("There is no category");
            message.setVisible(true);
            return;
        }



        ProductUtil util = new ProductUtil();
        if(EditUtil.isAvailable()){
            util.modProduct(EditUtil.getProduct().getId(),desc,priceValue,cat.getId());
            EditUtil.setProduct(null);
            EditUtil.setAvailable(false);
        }else{
            util.createProduct(desc, priceValue, cat.getId());
        }
        this.cancel();
        LogUtil.log("Product created");
        message.setText("Product Created Successfuly");
        message.setVisible(true);
    }


    public void cancel(){
        price.clear();
        description.clear();
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        this.category.setValue(null);
        this.addProduct.setText("Add Product");
    }

//    public void deleteCategory(){
//        System.out.println(this.category.getSelectionModel().getSelectedItem());
//        ViewHelper viewHelper = new ViewHelper();
//        GlobalValues.setSharedObject(this.category.getSelectionModel().getSelectedItem());
//        viewHelper.makeModal("deleteModal");
//        List<Category> categories = this.categoryUtil.allCategories();
//        this.category.setItems(FXCollections.observableArrayList(categories));
//
////        this.category.getItems().remove(this.category.getSelectionModel().getSelectedItem());
//    }


}
