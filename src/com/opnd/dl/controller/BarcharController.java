package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.ArrayList;
import java.util.List;

public class BarcharController {
    private @FXML BarChart<String, Number> barChart;
    private @FXML CategoryAxis xAxis;
    private @FXML NumberAxis yAxis;
    private @FXML JFXComboBox<Integer> yearCMB;

    public void initialize(){
        List<Integer> years = new ArrayList<>();
        for(int i = 2017; i <= 2038; i++) years.add(i);
        yearCMB.setItems(FXCollections.observableArrayList(years));
        yearCMB.getSelectionModel().select(0);
        List<String> months = new ArrayList<>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");
//        xAxis = new CategoryAxis();
        xAxis.setCategories(FXCollections.observableArrayList(months));
        xAxis.setLabel("Months");

//        yAxix = new NumberAxis();
        yAxis.setLabel("Sales");

        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("January");
        series1.getData().add(new XYChart.Data<>("January", 5));

        XYChart.Series<String, Number> series2 = new XYChart.Series<>();
        series2.setName("February");
        series2.getData().add(new XYChart.Data<>("February", 15));

        XYChart.Series<String, Number> series3 = new XYChart.Series<>();
        series3.getData().add(new XYChart.Data<>("March", 2));
        series3.setName("March");

        XYChart.Series<String, Number> series4 = new XYChart.Series<>();
        series4.getData().add(new XYChart.Data<>("April", 66));
        series4.setName("April");

        XYChart.Series<String, Number> series5 = new XYChart.Series<>();
        series5.getData().add(new XYChart.Data<>("May", 51));
        series5.setName("May");

        XYChart.Series<String, Number> series6 = new XYChart.Series<>();
        series6.getData().add(new XYChart.Data<>("June", 71));
        series6.setName("June");

        XYChart.Series<String, Number> series7 = new XYChart.Series<>();
        series7.getData().add(new XYChart.Data<>("July", 10));
        series7.setName("July");

        XYChart.Series<String, Number> series8 = new XYChart.Series<>();
        series8.getData().add(new XYChart.Data<>("August", 23));
        series8.setName("August");

        XYChart.Series<String, Number> series9 = new XYChart.Series<>();
        series9.getData().add(new XYChart.Data<>("September", 15));
        series9.setName("September");

        XYChart.Series<String, Number> series10 = new XYChart.Series<>();
        series10.getData().add(new XYChart.Data<>("October", 5));
        series10.setName("October");

        XYChart.Series<String, Number> series11 = new XYChart.Series<>();
        series11.getData().add(new XYChart.Data<>("November", 12));
        series11.setName("November");

        XYChart.Series<String, Number> series12 = new XYChart.Series<>();
        series12.getData().add(new XYChart.Data<>("December", 26));
        series12.setName("December");

        barChart.getData().addAll(series1, series2, series3, series4, series5, series6, series7, series8, series9, series10, series11, series12);
    }
}
