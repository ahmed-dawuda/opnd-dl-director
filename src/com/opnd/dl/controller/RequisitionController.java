package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.opnd.dl.domain.*;
import com.opnd.dl.domain.wrapper.DetailWrapper;
import com.opnd.dl.domain.wrapper.TransferRequestWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.RequisitionUtil;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequisitionController {
    @FXML private TableView<TransferRequestWrapper> requestTable;
    @FXML private TableColumn<TransferRequestWrapper,Date> dateCol;
    @FXML private TableColumn<TransferRequestWrapper, Subject> requestedByCol;
    @FXML private TableColumn<TransferRequestWrapper, Requisition.RequisitionStatus> statusCol;
    @FXML private TableColumn<TransferRequestWrapper, Location> locationCol;

    @FXML private TableView<DetailWrapper> detailTable;
    @FXML private TableColumn<DetailWrapper, Product> productCol;
    @FXML private TableColumn<DetailWrapper, Double> quantityCol;
    @FXML private TableColumn<DetailWrapper, Double> amountCol;

    @FXML private Label username;
    @FXML private Label role;

    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private JFXCheckBox endDateCB;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox personCB;
    @FXML private JFXComboBox<Subject> personCMB;
    @FXML private JFXCheckBox approved;
    @FXML private JFXCheckBox unapproved;

    private RequisitionUtil requisitionUtil;

    public void initialize(){
        this.requisitionUtil = new RequisitionUtil();
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");

        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
        personCMB.disableProperty().bind(personCB.selectedProperty().not());

        DecimalFormat df = new DecimalFormat("#,###,###.##");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        requestedByCol.setCellValueFactory(new PropertyValueFactory<>("requestedBy"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));

        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));

        dateCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        requestedByCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });


        statusCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Requisition.RequisitionStatus>(){
            @Override
            protected void updateItem(Requisition.RequisitionStatus item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item + "");
            }
        });


        productCol.setCellFactory(param -> new TableCell<DetailWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<DetailWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        amountCol.setCellFactory(param -> new TableCell<DetailWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        locationCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        requestTable.setItems(FXCollections.observableArrayList(requisitionUtil.listWrappedRequisitions()));

        requestTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            System.out.println("clicked");
            TransferRequestWrapper item =  requestTable.getSelectionModel().getSelectedItem();
            List<TransferItem> transferItems = Persistor.fetch("select t from TransferItem t where t.requisitionId = ?1",item.getRequisition().getId());
            List<DetailWrapper> detailWrappers = new ArrayList<>();
            for(TransferItem transferItem : transferItems){
                detailWrappers.add(new DetailWrapper(transferItem));
            }
            detailTable.setItems(FXCollections.observableArrayList(detailWrappers));
        });
    }

    @FXML private void approve(){
        GlobalValues.setRequisition(requestTable.getSelectionModel().getSelectedItem().getRequisition());
        new ViewHelper().makeModal("approveRequest");
        requestTable.setItems(FXCollections.observableArrayList(requisitionUtil.listWrappedRequisitions()));
    }

    @FXML private void cancel(){

    }

    public void filter(){

    }
}
