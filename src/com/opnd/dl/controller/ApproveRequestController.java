package com.opnd.dl.controller;

import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Role;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.ApprovalWrapper;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.RequisitionUtil;
import com.opnd.dl.model.UserUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class ApproveRequestController {

    @FXML private TableView<ApprovalWrapper> destinationTable;
    @FXML private TableColumn<ApprovalWrapper, Subject> personCol;
    @FXML private TableColumn<ApprovalWrapper, Location> locationCol;
    @FXML private TableColumn<ApprovalWrapper, Role> roleCol;
    @FXML private TableColumn<ApprovalWrapper, Boolean> hasEnoughCol;
    @FXML private Label message;

    private RequisitionUtil requisitionUtil = new RequisitionUtil();

    public void initialize(){
        personCol.setCellValueFactory(new PropertyValueFactory<>("person"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        roleCol.setCellValueFactory(new PropertyValueFactory<>("role"));
        hasEnoughCol.setCellValueFactory(new PropertyValueFactory<>("hasEnough"));

        personCol.setCellFactory(param -> new TableCell<ApprovalWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });

        locationCol.setCellFactory(param -> new TableCell<ApprovalWrapper, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        roleCol.setCellFactory(param -> new TableCell<ApprovalWrapper, Role>(){
            @Override
            protected void updateItem(Role item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        hasEnoughCol.setCellFactory(param -> new TableCell<ApprovalWrapper, Boolean>(){
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.booleanValue() ? "YES" : "NO");
            }
        });
        UserUtil userUtil = new UserUtil();
        List<Subject> subjects = userUtil.getSuppliers();
        List<ApprovalWrapper> approvalWrappers = new ArrayList<>();
        for (Subject subject : subjects) {
            approvalWrappers.add(new ApprovalWrapper(subject));
        }
        destinationTable.setItems(FXCollections.observableArrayList(approvalWrappers));
    }

    public void finish(){
        ApprovalWrapper selectedDestination = destinationTable.getSelectionModel().getSelectedItem();
        if( selectedDestination != null && selectedDestination.isHasEnough()){
            System.out.println("sending request to target");
            requisitionUtil.approveRequisition(selectedDestination.getPerson());
            message.setText("APPROVAL HAS BEEN SENT TO "+ selectedDestination.getPerson().getFullname().toUpperCase());
            ((Stage) message.getScene().getWindow()).close();
        }else{
            message.setText(selectedDestination.getPerson().getFullname().toUpperCase()+" DOES NOT HAVE ENOUGH INVENTORY");
            System.out.println("cannot send request to this target");
        }
    }
}
