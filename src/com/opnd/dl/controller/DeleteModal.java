package com.opnd.dl.controller;

import com.opnd.dl.domain.Category;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DeleteModal {

    @FXML private VBox modal;

    @FXML private void yes(){
        Category category = (Category) GlobalValues.getSharedObject();
        Persistor.begin();
        Category c = (Category) Persistor.find(Category.class, category.getId());
        c.setDeleted(true);
        Persistor.commit();
        this.no();
    }

    @FXML private void no(){
        ((Stage) this.modal.getScene().getWindow()).close();
    }
}
