package com.opnd.dl.controller;

import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.wrapper.ProductManagementWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.EditUtil;
import com.opnd.dl.model.ProductUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductManagementController {
    @FXML private TableView<ProductManagementWrapper> productManagementTable;
    @FXML private TableColumn<ProductManagementWrapper, Category> categoryCol;
    @FXML private TableColumn<ProductManagementWrapper, String> nameCol;
    @FXML private TableColumn<ProductManagementWrapper, Double> priceCol;
    @FXML private TableColumn<ProductManagementWrapper, String> deleteCol;
    @FXML private TableColumn<ProductManagementWrapper, String> editCol;

    @FXML private Label username;
    @FXML private Label role;

    private ProductUtil productUtil;

    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        editCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        this.productUtil = new ProductUtil();

        categoryCol.setCellFactory(param -> new TableCell<ProductManagementWrapper, Category>(){
            @Override
            protected void updateItem(Category item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        priceCol.setCellFactory(param -> new TableCell<ProductManagementWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : new DecimalFormat("#,###,###.##").format(item));
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<ProductManagementWrapper, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deleteLink = new Hyperlink("DELETE");
                deleteLink.setOnAction(e -> deleteProduct(item));
                deleteLink.setStyle("-fx-alignment: baseline-center");
                deleteLink.setStyle("-fx-text-fill: #E74C3C");
                setGraphic(item == null || empty ? null : deleteLink);
            }
        });

        editCol.setCellFactory(param -> new TableCell<ProductManagementWrapper, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink editLink = new Hyperlink("EDIT");
                editLink.setOnAction(e -> editProduct(item));
                editLink.setStyle("-fx-alignment: baseline-center");
                editLink.setStyle("-fx-text-fill: green");
                setGraphic(item == null || empty ? null : editLink);
            }
        });

        productManagementTable.setItems(FXCollections.observableArrayList(this.productUtil.allWrappedProducts()));
    }

    public void deleteProduct(String id){
        ProductManagementWrapper productManagementWrapper = null;
        for(ProductManagementWrapper product : productManagementTable.getItems()){
            if(product.getId().equalsIgnoreCase(id)){
                productManagementWrapper = product;
            }
        }

        productManagementTable.getItems().remove(productManagementWrapper);
        this.productUtil.deleteProduct(productManagementWrapper.getProduct());
    }

    public void editProduct(String id){
//        System.out.println("Editing product");
        EditUtil.setProduct((Product) Persistor.find(Product.class, id));
        EditUtil.setAvailable(true);
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("newProduct");
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        productManagementTable.setItems(FXCollections.observableArrayList(this.productUtil.allWrappedProducts()));
    }

    public void addNew(){
        ViewHelper viewHelper = new ViewHelper();
//        viewHelper.switchRoot("createProduct",productManagementTable);
        viewHelper.makeModal("newProduct");
        productManagementTable.setItems(FXCollections.observableArrayList(this.productUtil.allWrappedProducts()));
    }

    public void newCategory(){
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("newCategory");
        productManagementTable.setItems(FXCollections.observableArrayList(this.productUtil.allWrappedProducts()));
    }

}
