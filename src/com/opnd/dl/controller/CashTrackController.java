package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.CashTrackWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.model.PaymentUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

public class CashTrackController {
    @FXML private TableView<CashTrackWrapper> cashTrackTable;
    @FXML private TableColumn<CashTrackWrapper, Date> dateCol;
    @FXML private TableColumn<CashTrackWrapper, Double> amountCol;
    @FXML private TableColumn<CashTrackWrapper, Subject> personCol;
    @FXML private Label username;
    @FXML private Label role;
    @FXML private JFXCheckBox personCB;
    @FXML private JFXComboBox personCMB;
    private PaymentUtil paymentUtil = new PaymentUtil();


    public void initialize(){
        personCMB.disableProperty().bind(personCB.selectedProperty().not());
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        personCol.setCellValueFactory(new PropertyValueFactory<>("person"));

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        dateCol.setCellFactory(param -> new TableCell<CashTrackWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        amountCol.setCellFactory(param -> new TableCell<CashTrackWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item ==  null || empty ? null : df.format(item));
            }
        });

        personCol.setCellFactory(param -> new TableCell<CashTrackWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });

        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");

        cashTrackTable.setItems(FXCollections.observableArrayList(paymentUtil.cashTrackWrappers()));
    }
}
