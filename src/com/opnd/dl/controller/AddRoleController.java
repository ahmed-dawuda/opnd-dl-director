package com.opnd.dl.controller;

import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Role;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.RoleUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class AddRoleController {
    @FXML private JFXTextField name;
    @FXML private Label message;
    private RoleUtil roleUtil = new RoleUtil();

    public void initialize(){

    }

    public void add(){
        roleUtil.insert(name.getText());
        this.message.setText("Role Added Successfully");
        this.name.clear();
    }

    public void cancel(){
        name.clear();
    }
}
