package com.opnd.dl.controller;

import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Category;
import com.opnd.dl.model.CategoryUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class AddCategoryController {

    private CategoryUtil categoryUtil;
    @FXML private JFXTextField categoryName;
    @FXML private Label message;

    public void initialize(){
        this.categoryUtil = new CategoryUtil();
    }

    public void addCategory(){
        if(!categoryName.getText().isEmpty()){
            categoryUtil.insert(categoryName.getText());
            message.setText("Category added");
            cancel();
        }else {
            message.setText("Please Enter a category");
        }
    }

    public void cancel(){
        categoryName.clear();
    }
}
