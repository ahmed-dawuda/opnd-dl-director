package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.Persistor;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;

public class ReAssignRoleController {

    @FXML private JFXComboBox<Subject> worker;
    @FXML private JFXComboBox<Location> loc;

    public void initialize(){
        worker.setConverter(new StringConverter<Subject>() {
            @Override
            public String toString(Subject object) {
                return object.getFullname();
            }

            @Override
            public Subject fromString(String string) {
                return null;
            }
        });

        worker.setCellFactory(param -> new ListCell<Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });

        loc.setConverter(new StringConverter<Location>() {
            @Override
            public String toString(Location object) {
                return object.getDescription();
            }

            @Override
            public Location fromString(String string) {
                return null;
            }
        });

        loc.setCellFactory(param -> new ListCell<Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        worker.setItems(FXCollections.observableArrayList(Persistor.fetchAll("Subject")));
        loc.setItems(FXCollections.observableArrayList(Persistor.fetchAll("Location")));
    }

    public void add(){

    }

    public void cancel(){

    }
}
