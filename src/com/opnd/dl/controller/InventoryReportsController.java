package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.*;
import com.opnd.dl.domain.wrapper.InventoryWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.InventoryUtil;
import com.opnd.dl.model.LocationUtil;
import com.opnd.dl.model.UserUtil;
import com.opnd.dl.reports.CSV;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InventoryReportsController {

    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox endDateCB;
//    @FXML private JFXCheckBox locationCB;
//    @FXML private JFXComboBox locationCMB;
    @FXML private JFXCheckBox spCB;
    @FXML private JFXComboBox spCMB;

    @FXML private Label username;
    @FXML private Label role;

    @FXML private TableView<InventoryWrapper> inventoryReportTable;
    @FXML private TableColumn<InventoryWrapper, Date> dateCol;
    @FXML private TableColumn<InventoryWrapper, Location> locationCol;
    @FXML private TableColumn<InventoryWrapper, Product> productCol;
    @FXML private TableColumn<InventoryWrapper, Category> categoryCol;
    @FXML private TableColumn<InventoryWrapper, Double> amountCol;
    @FXML private TableColumn<InventoryWrapper, Double> quantityCol;
    @FXML private TableColumn<InventoryWrapper, Double> unitPriceCol;
    UserUtil userUtil = new UserUtil();


    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
//        locationCMB.disableProperty().bind(locationCB.selectedProperty().not());
        spCMB.disableProperty().bind(spCB.selectedProperty().not());


        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        unitPriceCol.setCellValueFactory(new PropertyValueFactory<>("unitprice"));

        locationCol.setCellFactory(param -> new TableCell<InventoryWrapper, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });


        dateCol.setCellFactory(param -> new TableCell<InventoryWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<InventoryWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        categoryCol.setCellFactory(param -> new TableCell<InventoryWrapper, Category>(){
            @Override
            protected void updateItem(Category item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        DecimalFormat df = new DecimalFormat("#,###,###.##");
        amountCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        unitPriceCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        spCMB.setCellFactory(param -> new ListCell<Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getEmail());
            }
        } );

//        locationCMB.setCellFactory(param -> new ListCell<Location>(){
//            @Override
//            protected void updateItem(Location item, boolean empty) {
//                super.updateItem(item, empty);
//                setText(item == null || empty ? null : item.getDescription());
//            }
//        });
//        LocationUtil locationUtil = new LocationUtil();
//        locationCMB.setItems(FXCollections.observableArrayList(locationUtil.allLocations()));

        spCMB.setItems(FXCollections.observableArrayList(userUtil.allSubject()));

        List<InventoryItem> inventoryitems = Persistor.fetchAll("InventoryItem");
        List<InventoryWrapper> inventoryWrappers = new ArrayList<>();
        for (InventoryItem inventoryItem : inventoryitems){
//            System.out.println(inve);
            InventoryWrapper wrap = new InventoryWrapper(inventoryItem);
            inventoryWrappers.add(wrap);
//            System.out.println(wrap.getLocation());
        }

        inventoryReportTable.setItems(FXCollections.observableArrayList(inventoryWrappers));

    }

    public void filter() {
        InventoryUtil inventoryUtil = new InventoryUtil();
        String query = "select i from InventoryItem i ";
        List<Object> params = new ArrayList<>();
        boolean isSelected = false;
        int arg = 1;

        if (startDateCB.isSelected()) {
            query += " where FUNC('DATE', i.date / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            params.add(startDateDP.getValue());
//            LogUtil.log(startDateDP.getValue().toString());
            isSelected = true;
        }
        if(endDateCB.isSelected()){
            query += isSelected ? " and FUNC('DATE', i.date  / 1000, 'unixepoch') <= ?2" : " where FUNC('DATE', i.date  / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            isSelected = true;
            params.add(endDateDP.getValue().toString());
        }

       if(spCB.isSelected() && spCMB.getValue() != null){
           query += isSelected ? " and i.holderId = ?"+arg : " where i.holderId = ?"+arg;
           arg++;
           params.add(((Subject) spCMB.getValue()).getId());
       }

        List<InventoryItem> filteredInventories = inventoryUtil.filteredInventories(query,params);
        List<InventoryWrapper> inventoryWrappers = new ArrayList<>();
        for (InventoryItem inventoryItem : filteredInventories){
            InventoryWrapper wrap = new InventoryWrapper(inventoryItem);
            inventoryWrappers.add(wrap);
        }

        inventoryReportTable.setItems(FXCollections.observableArrayList(inventoryWrappers));
    }

    public void export(){
        CSV csv = new CSV();
        List<InventoryItem> inventoryitems = Persistor.fetchAll("InventoryItem");
        List<InventoryWrapper> inventoryWrappers = new ArrayList<>();
        for (InventoryItem inventoryItem : inventoryitems){
            InventoryWrapper wrap = new InventoryWrapper(inventoryItem);
            inventoryWrappers.add(wrap);
        }
        csv.inventoryReport(inventoryWrappers);
        LogUtil.log("Inventory report generated");
    }
}
