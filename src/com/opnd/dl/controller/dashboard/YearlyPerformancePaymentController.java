package com.opnd.dl.controller.dashboard;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.model.PaymentUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class YearlyPerformancePaymentController {
    @FXML private CategoryAxis xAxis;
    @FXML private NumberAxis yAxis;
    @FXML private LineChart<String, Number> lineChart;
    @FXML private JFXComboBox<Integer> addyear;
    private List<Integer> removedyear = new ArrayList<>();
    private String CURRENT_YEAR;

    private PaymentUtil paymentUtil = new PaymentUtil();
    private List<Integer> years = new ArrayList<>();

    public void initialize(){
        SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");

        Date nowDate = new Date();
        String currentYear = formatNowYear.format(nowDate);
        CURRENT_YEAR = currentYear;

        for(int i = Integer.parseInt(currentYear); i >= 2015; i--) years.add(i);
        addyear.setItems(FXCollections.observableArrayList(years));
        addyear.getSelectionModel().select(new Integer(currentYear));
        removedyear.add(Integer.parseInt(currentYear));


        addyear.valueProperty().addListener((obs, oldSelection, newSelection)-> {
            boolean isAdded = false;
            for (Integer integer : removedyear) {
                if(integer.intValue() == newSelection.intValue()){
                    isAdded = true;
                }
            }

            if(!isAdded) {
                removedyear.add(newSelection);
                addYear(newSelection.intValue()+"");
                LogUtil.log(removedyear.toString());
            }
        });

        List<PaymentUtil.DataPair> pairs = paymentUtil.listPaymentsByMonths(currentYear);
        List<String> months = new ArrayList<>();
        for (PaymentUtil.DataPair pair : pairs) {
            months.add(pair.name);
        }
        xAxis.setLabel("Months");
        yAxis.setLabel("Payments (GHS)");
        xAxis.setCategories(FXCollections.observableArrayList(months));

        addYear(currentYear);
    }

    public void addYear(String year){
        List<PaymentUtil.DataPair> pairs = paymentUtil.listPaymentsByMonths(year);
        XYChart.Series series = new XYChart.Series();
        series.setName(year);
        for (PaymentUtil.DataPair pair : pairs) {
            series.getData().add(new XYChart.Data(pair.name,  pair.value));
        }
        lineChart.getData().add(series);
    }

    public void clear(){
        removedyear.clear();
        removedyear.add(Integer.parseInt(CURRENT_YEAR));
        addyear.getSelectionModel().select(Integer.parseInt(CURRENT_YEAR));
        lineChart.getData().clear();
        addYear(CURRENT_YEAR);
    }
}
