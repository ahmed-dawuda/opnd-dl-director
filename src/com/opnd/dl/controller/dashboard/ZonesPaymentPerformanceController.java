package com.opnd.dl.controller.dashboard;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.Role;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.ZonesPaymentPerformanceWrapper;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ZonesPaymentPerformanceController {
    @FXML private TableView<ZonesPaymentPerformanceWrapper> zonesTable;
    @FXML private TableColumn<ZonesPaymentPerformanceWrapper, Integer> rankCol;
    @FXML private TableColumn<ZonesPaymentPerformanceWrapper, String> nameCol;
    @FXML private TableColumn<ZonesPaymentPerformanceWrapper, String> locationCol;
    @FXML private TableColumn<ZonesPaymentPerformanceWrapper, Double> salesCol;
    @FXML private JFXComboBox<Integer> month;
    @FXML private JFXComboBox<Integer> year;

    private int CURRENT_YEAR;
    private int CURRENT_MONTH;

    public void initialize(){
        rankCol.setCellValueFactory(new PropertyValueFactory<>("rank"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        salesCol.setCellValueFactory(new PropertyValueFactory<>("sales"));

        Date nowDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowDate);
        CURRENT_YEAR = cal.get(Calendar.YEAR);
        CURRENT_MONTH = cal.get(Calendar.MONTH) + 1;

        month.setCellFactory(param -> new ListCell<Integer>(){
            @Override
            protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : makeMonth(item.intValue()));
            }
        });

        List<Integer> m = new ArrayList<>();
        for(int i = 1; i<= 12; i++) m.add(i);
        month.setItems(FXCollections.observableArrayList(m));
        month.getSelectionModel().select(new Integer(CURRENT_MONTH));

        List<Integer> y = new ArrayList<>();
        for(int j = CURRENT_YEAR; j >= 2015; j--) y.add(j);

        year.setItems(FXCollections.observableArrayList(y));
        year.getSelectionModel().select(new Integer(CURRENT_YEAR));

        year.valueProperty().addListener((obs, oldSelection, newSelection) -> {
            populateTable();
        });

        month.valueProperty().addListener((obs, oldSelection, newSelection) -> {
            populateTable();
        });

        populateTable();
    }

    public void populateTable(){
        int selectedMonth = month.getValue();
        int selectedYear = year.getValue();
        List roles = Persistor.fetch("Select r from Role r where r.description = ?1", "Zonal Officer");

        Role role = roles.size() > 0 ? (Role) roles.get(0) : null;

        List<Subject> subjects = role != null ? Persistor.fetch("Select s from Subject s where s.roleId =?1", role.getId()) : new ArrayList<>();

        List<ZonesPaymentPerformanceWrapper> list = new ArrayList<>();
        int rnk = 1;
        for (Subject subject : subjects) {
            list.add(new ZonesPaymentPerformanceWrapper(subject, rnk, selectedMonth, selectedYear));
            rnk++;
        }
        LogUtil.log(list.size()+"");
        int n = list.size();

        for(int pass = 1; pass < n; pass++){
            int pos = 1;
            for(int comp = 0; comp < n - 1; comp++){
                if(list.get(comp).getSales() < list.get(comp+1).getSales()){
                    ZonesPaymentPerformanceWrapper performanceWrapper = list.get(comp);
                    list.set(comp, list.get(comp+1));
                    list.set(comp+1, performanceWrapper);
                }
                if(pass == n - 1) {
                    list.get(comp).setRank(pos);
                    pos++;
                }
            }
        }
        zonesTable.setItems(FXCollections.observableArrayList(list));
    }

    public String makeMonth(int id){
        String name = "";
        switch (id){
            case 1:
                name = "January"; break;
            case 2:
                name = "February"; break;
            case 3:
                name = "March"; break;
            case 4:
                name = "April"; break;
            case 5:
                name = "May"; break;
            case 6:
                name = "June"; break;
            case 7:
                name = "July"; break;
            case 8:
                name = "August"; break;
            case 9:
                name = "September"; break;
            case 10:
                name = "October"; break;
            case 11:
                name = "November"; break;
            default:
                name = "December";
        }
        return name;
    }
}
