package com.opnd.dl.controller.dashboard;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.model.SaleUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ListCell;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class WeeklySalesPieController {
    @FXML private PieChart pieChart;
    @FXML private JFXComboBox<Integer> month;
    @FXML private JFXComboBox<Integer> year;
    private int CURRENT_YEAR;
    private int CURRENT_MONTH;
    private SaleUtil saleUtil = new SaleUtil();

    public void initialize(){

        Date nowDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowDate);
        CURRENT_YEAR = cal.get(Calendar.YEAR);
        CURRENT_MONTH = cal.get(Calendar.MONTH) + 1;

        month.setCellFactory(param -> new ListCell<Integer>(){
            @Override
            protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : makeMonth(item.intValue()));
            }
        });
        List<Integer> m = new ArrayList<>();
        for(int i = 1; i<= 12; i++) m.add(i);
        month.setItems(FXCollections.observableArrayList(m));
        month.getSelectionModel().select(new Integer(CURRENT_MONTH));

        List<Integer> y = new ArrayList<>();
        for(int j = CURRENT_YEAR; j >= 2015; j--) y.add(j);

        year.setItems(FXCollections.observableArrayList(y));
        year.getSelectionModel().select(new Integer(CURRENT_YEAR));

        year.valueProperty().addListener((obs, oldSelection, newSelection) -> {
            drawChart();
        });

        month.valueProperty().addListener((obs, oldSelection, newSelection) -> {
            drawChart();
        });


//        ObservableList<PieChart.Data> pieChartData =
//                FXCollections.observableArrayList(
//                        new PieChart.Data("Week 1", 13),
//                        new PieChart.Data("Week 2", 25),
//                        new PieChart.Data("Week 3", 10),
//                        new PieChart.Data("Week 4", 22));
//        pieChart.setData(pieChartData);
        drawChart();

    }

    public String makeMonth(int id){
        String name = "";
        switch (id){
            case 1:
                name = "January"; break;
            case 2:
                name = "February"; break;
            case 3:
                name = "March"; break;
            case 4:
                name = "April"; break;
            case 5:
                name = "May"; break;
            case 6:
                name = "June"; break;
            case 7:
                name = "July"; break;
            case 8:
                name = "August"; break;
            case 9:
                name = "September"; break;
            case 10:
                name = "October"; break;
            case 11:
                name = "November"; break;
            default:
                name = "December";
        }
        return name;
    }

    public void drawChart(){
        int selectedMonth = month.getValue();
        int selectedYear = year.getValue();
        ObservableList<PieChart.Data> sales = saleUtil.weeklySales(selectedMonth, selectedYear);
        pieChart.setData(sales);
    }
}
