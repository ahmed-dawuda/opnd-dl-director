package com.opnd.dl.controller.dashboard;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.model.SaleUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class YearlyPerformanceController {
    @FXML private CategoryAxis xAxis;
    @FXML private NumberAxis yAxis;
    @FXML private LineChart<String, Number> lineChart;
    @FXML private JFXComboBox<Integer> addyear;
    private List<Integer> removedyear = new ArrayList<>();
    private String CURRENT_YEAR;

    private SaleUtil saleUtil = new SaleUtil();
    private List<Integer> years = new ArrayList<>();

    public void initialize(){
        SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");

        Date nowDate = new Date();
        String currentYear = formatNowYear.format(nowDate);
        CURRENT_YEAR = currentYear;

        for(int i = Integer.parseInt(currentYear); i >= 2015; i--) years.add(i);
        addyear.setItems(FXCollections.observableArrayList(years));
        addyear.getSelectionModel().select(new Integer(currentYear));
        removedyear.add(Integer.parseInt(currentYear));


        addyear.valueProperty().addListener((obs, oldSelection, newSelection)-> {
            boolean isAdded = false;
            for (Integer integer : removedyear) {
                if(integer.intValue() == newSelection.intValue()){
                    isAdded = true;
                }
            }

            if(!isAdded) {
                removedyear.add(newSelection);
                addYear(newSelection.intValue()+"");
                LogUtil.log(removedyear.toString());
            }
        });

        List<SaleUtil.DataPair> pairs = saleUtil.listSalesByMonths("2017");
        List<String> months = new ArrayList<>();
        for (SaleUtil.DataPair pair : pairs) {
            months.add(pair.name);
        }
        xAxis.setLabel("Months");
        yAxis.setLabel("Sales (GHS)");
        xAxis.setCategories(FXCollections.observableArrayList(months));

        addYear(currentYear);
    }

    public void addYear(String year){
        List<SaleUtil.DataPair> pairs = saleUtil.listSalesByMonths(year);
        XYChart.Series series = new XYChart.Series();
        series.setName(year);
        for (SaleUtil.DataPair pair : pairs) {
            series.getData().add(new XYChart.Data(pair.name,  pair.value));
        }
        lineChart.getData().add(series);
    }

    public void clear(){
        removedyear.clear();
        removedyear.add(Integer.parseInt(CURRENT_YEAR));
        addyear.getSelectionModel().select(Integer.parseInt(CURRENT_YEAR));
        lineChart.getData().clear();
        addYear(CURRENT_YEAR);
    }
}
