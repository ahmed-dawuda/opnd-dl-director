package com.opnd.dl.controller.dashboard;

import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.model.PaymentUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentChartController {
    private @FXML BarChart<String, Number> barChart;
    private @FXML CategoryAxis xAxis;
    private @FXML NumberAxis yAxis;
    private @FXML JFXComboBox<Integer> yearCMB;
    private @FXML Label chartTitle;
    private PaymentUtil paymentUtil = new PaymentUtil();

    public void initialize(){

        SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");

        Date nowDate = new Date();
        String currentYear = formatNowYear.format(nowDate);

        List<PaymentUtil.DataPair> pairs = paymentUtil.listPaymentsByMonths(currentYear);

        List<Integer> years = new ArrayList<>();
        for(int i = Integer.parseInt(currentYear); i >= 2015; i--) years.add(i);
        yearCMB.setItems(FXCollections.observableArrayList(years));
        yearCMB.getSelectionModel().select(new Integer(currentYear));

        List<String> months = new ArrayList<>();
        for (PaymentUtil.DataPair pair : pairs) {
            months.add(pair.name);
        }
        xAxis.setCategories(FXCollections.observableArrayList(months));
        xAxis.setLabel("Months");
        yAxis.setLabel("Payments");
        makeChart(pairs, currentYear);

        yearCMB.valueProperty().addListener((obs, oldSelection, newSelection) -> {
            List<PaymentUtil.DataPair> dataPairs = paymentUtil.listPaymentsByMonths(newSelection+"");
            makeChart(dataPairs, ""+newSelection);
        });
    }

    public void makeChart(List<PaymentUtil.DataPair> pairs, String year){
        barChart.getData().clear();
        chartTitle.setText("ALL PAYMENTS ACROSS THE YEAR " + year);
        for (PaymentUtil.DataPair pair : pairs) {
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(pair.name);
            series.getData().add(new XYChart.Data<>(pair.name, pair.value));
            barChart.getData().add(series);
        }
    }
}
