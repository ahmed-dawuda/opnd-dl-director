package com.opnd.dl.controller;

import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.EditUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NavController {

    @FXML private VBox mainNode;
    private ViewHelper viewHelper;
    @FXML private Button maximise;

    public void initialize(){

        this.viewHelper = new ViewHelper();
    }

    public void home(){
        viewHelper.switchRoot("home",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void salesReport(){
        viewHelper.switchRoot("salesReport",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void inventoryReports(){
        viewHelper.switchRoot("inventoryReport",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void messaging(){
        viewHelper.switchRoot("messaging",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
    }

    public void paymentsReport(){
        viewHelper.switchRoot("paymentsReport",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void productManagement(){
        viewHelper.switchRoot("productManagement",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void logout(){
        viewHelper.switchScene("login",false,mainNode);
        GlobalValues.setCurrentSubject(null);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void requisition(){
        viewHelper.switchRoot("requisitions",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void workers(){
        viewHelper.switchRoot("workers",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }

    public void cashTrack(){
        viewHelper.switchRoot("cashTrack",mainNode);
        EditUtil.setProduct(null);
        EditUtil.setAvailable(false);
        EditUtil.setUser(null);
    }
}
