package com.opnd.dl.controller;

import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.ViewHelper;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HomeController {

    @FXML private Label username;
    @FXML private Label role;

    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
    }

}
