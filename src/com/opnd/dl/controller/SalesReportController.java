package com.opnd.dl.controller;


import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.SaleItem;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.SalesReportWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.SaleUtil;
import com.opnd.dl.model.UserUtil;
import com.opnd.dl.reports.CSV;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SalesReportController {
    @FXML private TableView<SalesReportWrapper> salesReportTable;
    @FXML private TableColumn<SalesReportWrapper, Date> dateCol;
    @FXML private TableColumn<SalesReportWrapper, Date> timeCol;
    @FXML private TableColumn<SalesReportWrapper, Subject> salespersonCol;
    @FXML private TableColumn<SalesReportWrapper, Product> productCol;
    @FXML private TableColumn<SalesReportWrapper, Double> amountCol;
    @FXML private TableColumn<SalesReportWrapper, Double> unitPriceCol;
    @FXML private TableColumn<SalesReportWrapper, Double> quantityCol;
    @FXML private TableColumn<SalesReportWrapper, Location> locationCol;

    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox endDateCB;
    @FXML private JFXCheckBox locationCB;
    @FXML private JFXComboBox locationCMB;
    @FXML private JFXCheckBox spCB;
    @FXML private JFXComboBox spCMB;

    @FXML private Label username;
    @FXML private Label role;

    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
//        locationCMB.disableProperty().bind(locationCB.selectedProperty().not());
        spCMB.disableProperty().bind(spCB.selectedProperty().not());


        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
        salespersonCol.setCellValueFactory(new PropertyValueFactory<>("salesperson"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        unitPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));

        //editing outputs
        dateCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getTimeInstance().format(item));
            }
        });

        salespersonCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });

        productCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        amountCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        unitPriceCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        locationCol.setCellFactory(param -> new TableCell<SalesReportWrapper, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        spCMB.setCellFactory(param -> new ListCell<Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getEmail());
            }
        });
        UserUtil userUtil = new UserUtil();
        spCMB.setItems(FXCollections.observableArrayList(userUtil.allSubject()));

        List<SaleItem> saleItems = Persistor.fetchAll("SaleItem");
        List<SalesReportWrapper> salesReportWrappers = new ArrayList<>();
        for(SaleItem saleItem : saleItems){
            salesReportWrappers.add(new SalesReportWrapper(saleItem));
        }
        salesReportTable.setItems(FXCollections.observableArrayList(salesReportWrappers));
    }

    public void filter() {
        SaleUtil saleUtil = new SaleUtil();
        String query = "select i from SaleItem i ";
        List<Object> params = new ArrayList<>();
        boolean isSelected = false;
        int arg = 1;

        if (startDateCB.isSelected()) {
            query += " where FUNC('DATE', i.date / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            params.add(startDateDP.getValue());
            isSelected = true;
        }
        if(endDateCB.isSelected()){
            query += isSelected ? " and FUNC('DATE', i.date  / 1000, 'unixepoch') <= ?2" : " where FUNC('DATE', i.date  / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            isSelected = true;
            params.add(endDateDP.getValue().toString());
        }

        if(spCB.isSelected() && spCMB.getValue() != null){
            query += isSelected ? " and i.holderId = ?"+arg : " where i.holderId = ?"+arg;
            arg++;
            params.add(((Subject) spCMB.getValue()).getId());
        }

        List<SaleItem> filteredSales = saleUtil.filteredSales(query,params);
        List<SalesReportWrapper> salesReportWrappers = new ArrayList<>();
        for(SaleItem saleItem : filteredSales){
            salesReportWrappers.add(new SalesReportWrapper(saleItem));
        }
        salesReportTable.setItems(FXCollections.observableArrayList(salesReportWrappers));
    }

    public void export(){
        CSV csv = new CSV();
        SaleUtil saleUtil = new SaleUtil();
        csv.saleReport(saleUtil.wrappedSales());
        System.out.println("sales report generated");
    }
}
