package com.opnd.dl.controller;

import com.opnd.dl.domain.Role;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.EditUtil;
import com.opnd.dl.model.LocationUtil;
import com.opnd.dl.model.RoleUtil;
import com.opnd.dl.model.UserUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class WorkerController {
    UserUtil userUtil;
    RoleUtil roleUtil;
    @FXML private TableView<Subject> workerTable;
    @FXML private TableColumn<Subject, String> fullnameCol;
    @FXML private TableColumn<Subject, String> usernameCol;
    @FXML private TableColumn<Subject, String> roleCol;
    @FXML private TableColumn<Subject, String> editCol;
    @FXML private TableColumn<Subject, String> deleteCol;
    @FXML private TableColumn<Subject, String> passwordCol;
    @FXML private TableColumn<Subject, String> locationCol;

    @FXML private TableView<Role> roleTable;
    @FXML private TableColumn<Role, String> roleDescCol;
    @FXML private TableColumn<Role, String> roleDelCol;

    public void initialize(){
        this.userUtil = new UserUtil();
        roleUtil = new RoleUtil();
        fullnameCol.setCellValueFactory(new PropertyValueFactory<>("fullname"));
        usernameCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        roleCol.setCellValueFactory(new PropertyValueFactory<>("roleId"));
        editCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        passwordCol.setCellValueFactory(new PropertyValueFactory<>("raw_password"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("locationId"));

        roleDelCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        roleDescCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        locationCol.setCellFactory(param -> new TableCell<Subject, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                LocationUtil locationUtil = new LocationUtil();
                setText(item == null || empty ? null : locationUtil.find(item).getDescription());
            }
        });

        roleDelCol.setCellFactory(param -> new TableCell<Role, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deleteRole = new Hyperlink("DELETE");
                deleteRole.setOnAction(e -> deleteRole(item));
                deleteRole.setStyle("-fx-alignment: baseline-center");
                deleteRole.setStyle("-fx-text-fill: #E74C3C");
                setGraphic(item == null || empty ? null : deleteRole);
            }
        });

        roleCol.setCellFactory(param -> new TableCell<Subject, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : userUtil.getRole(item).getDescription());
            }
        });

        editCol.setCellFactory(param -> new TableCell<Subject, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink editLink = new Hyperlink("EDIT");
                editLink.setOnAction(e -> edit(item));
                editLink.setStyle("-fx-alignment: baseline-center");
                editLink.setStyle("-fx-text-fill: green");
                setGraphic(item == null || empty ? null : editLink);
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<Subject, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deleteLink = new Hyperlink("DELETE");
                deleteLink.setOnAction(e -> delete(item));
                deleteLink.setStyle("-fx-alignment: baseline-center");
                deleteLink.setStyle("-fx-text-fill: #E74C3C");
                setGraphic(item == null || empty ? null : deleteLink);
            }
        });

        workerTable.setItems(FXCollections.observableArrayList(userUtil.allSubject()));
        roleTable.setItems(FXCollections.observableArrayList(roleUtil.allRoles()));
    }

    public void deleteRole(String item){
        Role toDelete = roleUtil.find(item);
        roleTable.getItems().remove(toDelete);
//        Persistor.begin();
//        toDelete.s
    }

    public void edit(String username){
        EditUtil.setUser((Subject) Persistor.find(Subject.class, username));
    }

    public void delete(String username){
        Subject toDelete = null;
        for(Subject user : workerTable.getItems()){
            if(user.getEmail().equalsIgnoreCase(username)){
                toDelete = user;
            }
        }

        workerTable.getItems().remove(toDelete);
        Persistor.begin();
        toDelete.setDeleted(true);
        Persistor.commit();

    }

    @FXML private void addworker(){
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("addWorker");
        workerTable.setItems(FXCollections.observableArrayList(userUtil.allSubject()));
    }

    @FXML private void addRole(){
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("addRole");
        roleTable.setItems(FXCollections.observableArrayList(roleUtil.allRoles()));
    }

    @FXML private void refresh(){
        //this method will synchronize the localdb with the onlinedb
    }

    @FXML private void addLocation(){
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("addLocation");
    }

    @FXML private void reassign(){
        ViewHelper viewHelper = new ViewHelper();
        viewHelper.makeModal("reassignlocation");
    }
}
