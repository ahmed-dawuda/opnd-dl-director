package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.NewStockItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.ProductUtil;
import com.opnd.dl.model.TransferUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public class NewStockController {

    //form fields
    @FXML private JFXTextField amount;
    @FXML private JFXTextField quantity;
    @FXML private JFXComboBox<Product> product;

    @FXML private Label username;
    @FXML private Label role;

    //table
    @FXML private TableView<NewStockItem> stockTable;
    @FXML private TableColumn<NewStockItem, Product> productCol;
    @FXML private TableColumn<NewStockItem, Double> amountCol;
    @FXML private TableColumn<NewStockItem, Double> quantityCol;
    @FXML private TableColumn<NewStockItem, Product> deleteCol;

    private double qty = 0;
    private double amt = 0;
    private Product p;

    //initializing
    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("product"));

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        amountCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<NewStockItem, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : item.getDescription());
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<NewStockItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deletLink =  new Hyperlink("DELETE");
                deletLink.setOnAction(e -> deleteRow(item));
                deletLink.setStyle("-fx-alignment: baseline-center");
                setGraphic(item == null || empty ? null : deletLink);
            }
        });

        product.setCellFactory(param -> new ListCell<Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });


        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;

            if (!newValue.matches("^[+-]?\\d+$")) {
                quantity.setText(oldValue);
                return;
            }

            qty = Double.parseDouble(newValue);
            amt = qty * p.getPrice();
            amount.setText(df.format(amt));
        });

        amount.setEditable(false);
        product.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> p = newValue);

//        ProductUtil util = new ProductUtil();
//        product.setItems(FXCollections.observableArrayList(util.allProducts()));
//        product.setItems(fewProducts());
//        if (product.getItems().size() > 0) product.getSelectionModel().select(0);

        stockTable.setItems(FXCollections.observableArrayList());

//        stockTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//            System.out.println("clicked");
//            NewStockItem item =  stockTable.getSelectionModel().getSelectedItem();
//            System.out.println(item.getProduct().getDescription());
//        });
    }

//    private ObservableList<Product> fewProducts() {
//        ObservableList<Product> products = FXCollections.observableArrayList();
//
//        Product product = new Product();
//        product.setDescription("Knapsack Sprayer");
//        product.setPrice(500);
//        product.setCategoryId(1);
//        product.setId(1);
//        products.add(product);
////        Persistor.insert(product);
//
//        Product prod = new Product();
//        prod.setDescription("Fertilizer");
//        prod.setId(2);
//        prod.setCategoryId(2);
//        prod.setPrice(1200);
//        products.add(prod);
//
//        Product p = new Product();
//        p.setPrice(58);
//        p.setCategoryId(2);
//        p.setId(3);
//        p.setDescription("Akate Global");
//        products.add(p);
//
//        return products;
//    }

    @FXML private void deleteRow(Product item){
        for (NewStockItem newStockItem : stockTable.getItems()) {
            if(newStockItem.getProduct().getId() == item.getId()){
                stockTable.getItems().remove(newStockItem);
            }
        }
    }

    @FXML private void add(){
        if(amt != 0 && qty != 0 && p != null){
            NewStockItem item = new NewStockItem();
            item.setAmount(amt);
            item.setProduct(p);
            item.setQuantity(qty);
            stockTable.getItems().add(item);
            this.quantity.setText("");
            this.amount.setText("");

        }
    }

    @FXML private void cancel(){
        stockTable.getItems().clear();
    }

    @FXML private void finish(){
        if(stockTable.getItems().size() > 0){
            for(NewStockItem item : stockTable.getItems()){
               TransferUtil.insert(item);
                this.cancel();
            }
        }
    }
}
