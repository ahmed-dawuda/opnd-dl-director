package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Payment;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.PaymentsReportWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.model.PaymentUtil;
import com.opnd.dl.reports.CSV;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentsReportController {
    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox endDateCB;
//    @FXML private JFXCheckBox locationCB;
//    @FXML private JFXComboBox locationCMB;
    @FXML private JFXCheckBox pbCB;
    @FXML private JFXComboBox pbCMB;

    @FXML private Label username;
    @FXML private Label role;

    @FXML private TableView<PaymentsReportWrapper> paymentsReportTable;
    @FXML private TableColumn<PaymentsReportWrapper, Date> dateCol;
    @FXML private TableColumn<PaymentsReportWrapper, Date> timeCol;
    @FXML private TableColumn<PaymentsReportWrapper, Location> locationCol;
    @FXML private TableColumn<PaymentsReportWrapper, Subject> paidByCol;
    @FXML private TableColumn<PaymentsReportWrapper, Double> amountCol;
    @FXML private TableColumn<PaymentsReportWrapper, Requisition.RequisitionStatus> statusCol;

    private PaymentUtil paymentUtil = new PaymentUtil();

    public void initialize(){
        this.username.setText(GlobalValues.getCurrentSubject().getFullname());
        this.role.setText("DIRECTOR");
        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
//        locationCMB.disableProperty().bind(locationCB.selectedProperty().not());
        pbCMB.disableProperty().bind(pbCB.selectedProperty().not());

        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        paidByCol.setCellValueFactory(new PropertyValueFactory<>("paidBy"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));

        dateCol.setCellFactory(param -> new TableCell<PaymentsReportWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(param -> new TableCell<PaymentsReportWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getTimeInstance().format(item));
            }
        });

        paidByCol.setCellFactory(param -> new TableCell<PaymentsReportWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });

        locationCol.setCellFactory(param -> new TableCell<PaymentsReportWrapper, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        amountCol.setCellFactory(param -> new TableCell<PaymentsReportWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : (new DecimalFormat("#,###,###.##")).format(item));
            }
        });

        paymentsReportTable.setItems(FXCollections.observableArrayList(paymentUtil.wrappedPayments()));

    }

    public void filter() {
        PaymentUtil paymentUtil = new PaymentUtil();
        String query = "select i from Payment i ";
        List<Object> params = new ArrayList<>();
        boolean isSelected = false;
        int arg = 1;

        if (startDateCB.isSelected()) {
            query += " where FUNC('DATE', i.date / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            params.add(startDateDP.getValue());
            isSelected = true;
        }
        if(endDateCB.isSelected()){
            query += isSelected ? " and FUNC('DATE', i.date  / 1000, 'unixepoch') <= ?2" : " where FUNC('DATE', i.date  / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            isSelected = true;
            params.add(endDateDP.getValue().toString());
        }

        if(pbCB.isSelected() && pbCMB.getValue() != null){
            query += isSelected ? " and i.holderId = ?"+arg : " where i.holderId = ?"+arg;
            arg++;
            params.add(((Subject) pbCMB.getValue()).getId());
        }

        List<Payment> filteredPayments = paymentUtil.filteredPayments(query,params);
        List<PaymentsReportWrapper> paymentsReportWrappers = new ArrayList<>();
        for(Payment payment : filteredPayments){
            paymentsReportWrappers.add(new PaymentsReportWrapper(payment));
        }
        paymentsReportTable.setItems(FXCollections.observableArrayList(paymentsReportWrappers));
    }

    public void export(){
        CSV csv = new CSV();
        csv.paymentReport(paymentUtil.wrappedPayments());
        System.out.println("payment report generated");
    }
}
