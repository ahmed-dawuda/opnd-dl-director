package com.opnd.dl.controller;

import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Location;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.model.LocationUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class AddLocationController {
    @FXML private JFXTextField name;
    @FXML private Label message;
    private LocationUtil locationUtil;

    public void initialize(){
        locationUtil = new LocationUtil();
    }

    public void add(){
        if(!name.getText().isEmpty()){
            Location location = new Location();
            location.setId(GlobalValues.generateId());
            location.setDescription(name.getText());
            locationUtil.insert(location);
            this.message.setText("Location Added");
            cancel();
        }else{
            this.message.setText("Please make sure all fields are specified");
        }
    }

    public void cancel(){
        name.clear();
    }
}
