package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Role;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class UserUtil {

//    private String holderId = GlobalValues.getCurrentSubject().getUsername();
//    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();


    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select s.id from Subject s");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Subject localFetchSubject(String id) {
        return (Subject) Persistor.find(Subject.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "users/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Subject remoteFetchSubject(String id) throws UnirestException {
        String url = BASE_URL + "users/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Subject.class);
        }

        return null;
    }

    public Subject[] downloadSubjects() throws UnirestException {
        String url = BASE_URL + "users";
//        LogUtil.log("downloadSubjects() is running...");

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray itemJson = object.getJSONArray("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Subject[].class);
        }
        return null;
    }

    private boolean downloadSubject(String id) throws UnirestException {
        Subject item = remoteFetchSubject(id);
//        LogUtil.log("ahmed - "+item);
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postSubject(String id) throws UnirestException {
        String url = BASE_URL + "register";
        Subject item = localFetchSubject(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
        boolean posted = object.has("status") && object.getBoolean("status");
        if(posted){
            Persistor.begin();
            item.setSynced(true);
            Persistor.commit();
        }
        return posted;
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();
        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

        for (String number : remoteList) {
            List<Subject> n = Persistor.fetch("select s from Subject s where s.id = ?1", number);
            if(n.size() < 1){
                remoteOnly.add(number);
            }
        }

//        LogUtil.log("users remote only - "+remoteOnly);

        for (String number : remoteOnly) {
            downloadSubject(number);
        }
//        if(localList.size() <= 2){
//            List<Subject> subjects = Arrays.asList(downloadSubjects());
//            for (Subject subject : subjects) {
//                if(!localList.contains(subject.getId())){
//                    subject.setSynced(true);
//                    Persistor.insert(subject);
//                }
//            }
//        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List<String> l = Persistor.fetch("select s.id from Subject s where s.synced = ?1", false);
//        LogUtil.log(l.toString());
        for (String id : l) {

            if(postSubject(id)){
                Subject c = localFetchSubject(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }

    public Subject login(String username, String password) {
//        createAdmin();

        Subject current = localFetch(username);
        if (current != null)
            if (current.getRaw_password().equalsIgnoreCase(password))
                return current;

        try {
            current = remoteFetch(username, password);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        if (current != null) {
            if (localFetch(current.getEmail()) == null) {
//                current.setId(0);
//                current.setEmail(username);
//                current.setPassword(password);
//                Role role = new RoleUtil().find(current.getRoleId());
//                if (role == null) {
//                    List<Role> admins =  Persistor.fetch("Select r from Role r where r.description = ?1","Admin");
//                    Role r ;
//                    if(admins.size() > 0){
//                        r = (Role) admins.get(0);
//                        current.setRoleId(r.getId());
//                    }
//
//                }
                Persistor.insert(current);
            }
//            if (current.getPassword().equalsIgnoreCase(password))
            return current;
        }

        return null;
    }

    private Subject remoteFetch(String username) {


        return null;
    }

    private Subject remoteFetch(String email, String password) throws UnirestException {
        final HttpResponse<JsonNode> response1 = Unirest.post(BASE_URL + "login")
                .header("Accept", "application/json")
                .field("email", email)
                .field("password", password)
//                .fields(m.convertValue(user, Map.class))
                .asJson();

        JSONObject object = response1.getBody().getObject();
        Subject subject = null;
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject message = object.getJSONObject("message");
//            LogUtil.log(message.toString(2));
            subject = new Gson().fromJson(message.toString(), Subject.class);
        }

//        LogUtil.log(object.toString(2));
        return subject;
    }


    private Subject localFetch(String email) {
        List l = Persistor.fetch("select s from Subject s where s.email = ?1", email);
        if (l.size() == 0) return null;
        else return (Subject) l.get(0);
    }

    public Subject createSubject(String firstname, String middlename, String lastname, String level, String password, String locationId) {
        Subject subject = new Subject();
        subject.setId(GlobalValues.generateId("Subject"));
        subject.setFirstname(firstname);

        StringBuilder builder = new StringBuilder(lastname);
        if (!middlename.isEmpty()) builder.append(" ").append(middlename);
        builder.append(" ").append(firstname);
        subject.setFullname(builder.toString());
        subject.setEmail(Character.toLowerCase(firstname.charAt(0)) + lastname.toLowerCase());
        subject.setRoleId(level);
        subject.setPassword(password);
        subject.setLocationId(locationId);
        subject.setRaw_password(password);

        Persistor.insert(subject);
//        new Thread(() -> {
//            try {
//                postUnsynced();
//            } catch (UnirestException e) {
//                e.printStackTrace();
//            }
//        }).start();
        return subject;
    }

    private Subject modSubject(String username, String firstname, String middlename, String lastname, String level, String password) {
        Persistor.begin();

        Subject subject = (Subject) Persistor.find(Subject.class, username);
        subject.setFirstname(firstname);

        StringBuilder builder = new StringBuilder(lastname);
        if (!middlename.isEmpty()) builder.append(" ").append(middlename);
        builder.append(" ").append(firstname);
        subject.setFullname(builder.toString());
        subject.setRoleId(level);
        subject.setPassword(password);

        Persistor.commit();

        return subject;
    }

//    private void createAdmin() {
//        List list = Persistor.fetchAll("Subject");
//        if (list.size() > 0) return;
//
//        Role role = new Role();
//        role.setDescription("Admin");
//        Persistor.insert(role);
//
//        Location location = new Location();
//        location.setDescription("main");
//        Persistor.insert(location);
//
//        Subject admin = createSubject("Nii", "", "Okai", role.getId(), "admin",location.getId());
//
//        Persistor.begin();
//        admin.setSynced(true);
//        Persistor.commit();
////        Persistor.insert(admin);
//    }

    public Role getRole(String roleId){
        return (Role) Persistor.find(Role.class, roleId);
    }

    public List<Subject> allSubject(){
        Role admin = (Role) Persistor.fetch("select r from Role r where r.description = ?1","admin").get(0);
        return Persistor.fetch("select s from Subject s where s.deleted = ?1 and s.roleId != ?2",false, admin.getId());
    }

    public List<Subject> getSuppliers(){
        Role role = (Role) Persistor.fetch("Select r from Role r where r.description = ?1", "Warehouse").get(0);
        List<Subject> suppliers = Persistor.fetch("Select s from Subject s where s.roleId = ?1", role.getId());
        return suppliers;
    }
}
