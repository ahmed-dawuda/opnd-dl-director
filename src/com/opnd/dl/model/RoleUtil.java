package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Role;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoleUtil {

//    private String holderId = GlobalValues.getCurrentSubject().getEmail();
//    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();


    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select r.id from Role r");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Role localFetchRole(String id) {
        return (Role) Persistor.find(Role.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "roles/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Role remoteFetchRole(String id) throws UnirestException {
        String url = BASE_URL + "roles/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Role.class);
        }

        return null;
    }

    private boolean downloadRole(String id) throws UnirestException {
        Role item = remoteFetchRole(id);
       if(item != null){
           item.setSynced(true);
           try {
               Persistor.insert(item);
               return true;
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
        return false;
    }

    private boolean postRole(String id) throws UnirestException {
        String url = BASE_URL + "roles";
        Role item = localFetchRole(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();
//        LogUtil.log("RemoteList for Role - "+remoteList.toString());
//        LogUtil.log("LocalList for Role - "+localList.toString());

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

//        for (Number number : localList) {
//            if (!remoteList.contains(number))
//                localOnly.add(number);
//        }
//        LogUtil.log("localOnly - "+localOnly.toString());
//
//        for (Number number : remoteList) {
//            if (!localList.contains(number))
//                remoteOnly.add(number);
//        }
//        LogUtil.log("remote only - "+remoteOnly.toString());

        for (String number : remoteList) {
            List<Role> n = Persistor.fetch("select s from Role s where s.id = ?1", number);
            if(n.size() < 1){
                remoteOnly.add(number);
            }
        }

//        for (Number number : localOnly) {
//            postRole(number);
//        }
//
        for (String number : remoteOnly) {
            downloadRole(number);
        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List<String> l = Persistor.fetch("select r.id from Role r where r.synced = ?1", false);
        for (String id : l) {
            if(postRole(id)){
                Role c = localFetchRole(id);
                try{
                    Persistor.begin();
                    c.setSynced(true);
                    Persistor.commit();
                }catch (Exception e){

                }
            }
        }
    }


    public void insert(String description){
        Role role = new Role();
        role.setId(GlobalValues.generateId());
        role.setDescription(description);
        Persistor.insert(role);
    }
    public List<Role> allRoles(){
        return Persistor.fetch("select r from Role r where r.description != ?1", "Admin");
    }

    public Role find(String id){
        return (Role) Persistor.find(Role.class, id);
    }
}
