package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Payment;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.CashTrackWrapper;
import com.opnd.dl.domain.wrapper.PaymentsReportWrapper;
import com.opnd.dl.mStuff.GlobalValues;
//import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PaymentUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    protected class IdHolderId {
        private String id;
        private String holderId;

        public IdHolderId(Object[] o) {
            id = (String) o[0];
            holderId = (String) o[1];
        }

        public boolean isInList(List<IdHolderId> list) {

            for (IdHolderId idHolderId : list)
                if (id.equalsIgnoreCase(idHolderId.getId()) && holderId.equalsIgnoreCase(idHolderId.holderId))
                    return true;

            return false;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHolderId() {
            return holderId;
        }

        public void setHolderId(String holderId) {
            this.holderId = holderId;
        }

        @Override
        public String toString() {
            return "id="+id+", holderId="+holderId;
        }
    }

    private List<IdHolderId> localFetchIds() {
        List<IdHolderId> ids = new ArrayList<>();
        List l = Persistor.fetch("select p.id, p.holderId from Payment p");

        for (Object o : l) {
            ids.add(new IdHolderId((Object[]) o));
        }

        return ids;
    }

    private Payment localFetchPayment(String id) {
        return (Payment) Persistor.find(Payment.class, id);
    }

    private List<IdHolderId> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "payments/idsOnly";
        List<IdHolderId> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
//            LogUtil.log(object.toString(2));
            for (int i = 0; i < idListJson.length(); i++) {
//                LogUtil.log("Ahmed: "+idListJson.getJSONObject(i).toString());
                try{
                    ids.add(new IdHolderId(new Object[]{idListJson.getJSONObject(i).getString("id"), idListJson.getJSONObject(i).getString("holderId")}));
                }catch (JSONException ex){
                    ex.printStackTrace();
                }
            }

        }
        return ids;
    }

    private Payment remoteFetchPayment(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "payments/" + id.getId() + "/" + id.getHolderId();
//        LogUtil.log(url);
        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
//            LogUtil.log(object.toString(2));
            JSONObject itemJson = null;
            try {
                itemJson = object.getJSONObject("message");
            }catch (JSONException e){
//                LogUtil.log(e.toString());
            }
            Payment item = null;
            if(itemJson != null){
                item = new Payment();
                item.setAmount(itemJson.getDouble("amount"));
                item.setDate(new Date(itemJson.getLong("date")));
                item.setHolderID(itemJson.getString("holderId"));
                item.setId(itemJson.getString("id"));
            }
//            Gson gson = new Gson();
//            return gson.fromJson(itemJson.toString(), InventoryItem.class);
            return item;
        }

        return null;
    }

    private boolean downloadPayment(IdHolderId id) throws UnirestException {
        Payment item = remoteFetchPayment(id);
//        LogUtil.log(item.toString());
//        return false;
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postPayment(String id) throws UnirestException {
        String url = BASE_URL + "payments";
        Payment item = localFetchPayment(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<IdHolderId> localList = localFetchIds();
        List<IdHolderId> remoteList = remoteFetchIds();

//        List<IdHolderId> localOnly = new ArrayList<>();
        List<IdHolderId> remoteOnly = new ArrayList<>();
//
//        for (IdHolderId number : localList) {
//            if (!number.isInList(remoteList))
//                localOnly.add(number);
//        }

        for (IdHolderId number : remoteList) {
            if (!number.isInList(localList))
                remoteOnly.add(number);
        }

//        for (IdHolderId number : localOnly) {
//            postRequisition(number);
//        }

        for (IdHolderId number : remoteOnly) {
            downloadPayment(number);
        }

//        LogUtil.log("PAYMENT_UTIL IS CALLED");
    }


    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List<String> l = Persistor.fetch("select p.id from Payment p where p.synced = ?1", false);
        for (String id : l) {
            if(postPayment(id)){
                Payment c = localFetchPayment(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }

    public List<Payment> payments(){
        return Persistor.fetch("Select p from Payment p");
    }

    public List<PaymentsReportWrapper> wrappedPayments(){
        List<Payment> allPayments = payments();
        List<PaymentsReportWrapper> paymentWrappers = new ArrayList<>();
        for (Payment payment : allPayments) {
            paymentWrappers.add(new PaymentsReportWrapper(payment));
        }
        return paymentWrappers;
    }

    public List<CashTrackWrapper> cashTrackWrappers(){
        List<CashTrackWrapper> cashTrackWrappers = new ArrayList<>();
        List<Number> list = Persistor.fetch("Select distinct s.holderId from SaleItem s");
        for (Number number : list) {
            Subject subject = (Subject) Persistor.find(Subject.class, number.intValue());
            cashTrackWrappers.add(new CashTrackWrapper(subject));
        }
        return cashTrackWrappers;
    }

    public List<Payment> filteredPayments(String query, List<Object> params){
        return Persistor.fetch(query, params.toArray());
    }

    public List<DataPair> listPaymentsByMonths(String year) {
        List<DataPair> pairs = new ArrayList<>();
        for(int i = 1; i <= 12; i++){
            pairs.add(monthlyPayments(i, year));
        }
        return  pairs;
    }

    public class DataPair{
        public String name = "";
        public double value = 0;

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(double value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "{" +
                    "name='" + name + '\'' +
                    ", value=" + value +
                    '}';
        }
    }

    public DataPair monthlyPayments(int month, String year){
        String date = month < 10 ? year +"-0"+ month + "-" : year +"-"+ month + "-";
        String query = "select sum(p.amount) from Payment p where FUNC('DATE', p.date, 'unixepoch') >= ?1 and FUNC('DATE', p.date, 'unixepoch') <= ?2";
        List payments = Persistor.fetch(query, date + "01", date + "31");
        String name = "";
        switch (month){
            case 1:
                name = "January"; break;
            case 2:
                name = "February"; break;
            case 3:
                name = "March"; break;
            case 4:
                name = "April"; break;
            case 5:
                name = "May"; break;
            case 6:
                name = "June"; break;
            case 7:
                name = "July"; break;
            case 8:
                name = "August"; break;
            case 9:
                name = "September"; break;
            case 10:
                name = "October"; break;
            case 11:
                name = "November"; break;
            default:
                name = "December";
        }
        DataPair d = new DataPair();
        d.setName(name);
        d.setValue(payments.get(0) == null ? 0 : (Double) payments.get(0));
        return d;
    }

    public ObservableList<PieChart.Data> weeklyPayments(int month, int year){
        String date = month < 10 ? year +"-0"+ month + "-" : year +"-"+ month + "-";
        ObservableList<PieChart.Data> paymentList = FXCollections.observableArrayList();
        int week = 1;
        for(int day = 1; day < 29; day+=7){
            String query = "select sum(s.amount) from Payment s where FUNC('DATE', s.date, 'unixepoch') >= ?1 and FUNC('DATE', s.date, 'unixepoch') <= ?2";
            List sales = Persistor.fetch(query, date + day, date + (day + 6));
            paymentList.add(new PieChart.Data("Week "+week, sales.get(0) == null ? 0 : (Double) sales.get(0)));
            week++;
        }
        return paymentList;
    }
}
