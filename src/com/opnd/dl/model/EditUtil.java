package com.opnd.dl.model;

import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.Subject;

public class EditUtil {
    private static Product product;
    private static boolean available = false;
    private static Subject user = null;
    private static boolean userSet = false;

    public static Subject getUser() {
        return user;
    }

    public static void setUser(Subject user) {
        EditUtil.user = user;
    }

    public static boolean isUserSet() {
        return userSet;
    }

    public static void setUserSet(boolean userSet) {
        EditUtil.userSet = userSet;
    }

    public static Product getProduct() {
        return product;
    }

    public static void setProduct(Product product) {
        EditUtil.product = product;
    }

    public static boolean isAvailable() {
        return available;
    }

    public static void setAvailable(boolean available) {
        EditUtil.available = available;
    }
}
