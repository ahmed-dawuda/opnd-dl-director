package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Location;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LocationUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();


    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select l.id from Location l");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Location localFetchLocation(String id) {
        return (Location) Persistor.find(Location.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "locations/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Location remoteFetchLocation(String id) throws UnirestException {
        String url = BASE_URL + "locations?id=" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray itemJson = object.getJSONArray("message");
            Gson gson = new Gson();
//            System.out.println(gson.fromJson(itemJson.get(0).toString(), Location.class));;
            return gson.fromJson(itemJson.get(0).toString(), Location.class);
        }

        return null;
    }

    public Location[] downloadLocations() throws UnirestException {
        String url = BASE_URL + "locations";
//        LogUtil.log("downloadlocations() is running...");

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray itemJson = object.getJSONArray("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Location[].class);
        }
        return null;
    }

    private boolean downloadLocation(String id) throws UnirestException {
        Location item = remoteFetchLocation(id);
//        LogUtil.log("Location - download - "+item.toString());
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postLocation(String id) throws UnirestException {
        String url = BASE_URL + "locations";
        Location item = localFetchLocation(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();
//
        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

        for (String number : remoteList) {
            List<Location> n = Persistor.fetch("select s from Location s where s.id = ?1", number);
            if(n.size() < 1){
                remoteOnly.add(number);
            }
        }

//        LogUtil.log("Location - "+remoteOnly);

//        LogUtil.log("users remote only - "+remoteOnly);

        for (String number : remoteOnly) {
            downloadLocation(number);
        }
//
//        for (Number number : localList) {
//            if (!remoteList.contains(number))
//                localOnly.add(number);
//        }
//
//        for (Number number : remoteList) {
//            if (!localList.contains(number))
//                remoteOnly.add(number);
//        }
//
//        for (Number number : localOnly) {
//            postLocation(number);
//        }
//
//        for (Number number : remoteOnly) {
//            downloadLocation(number);
//        }


    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List<String> l = Persistor.fetch("select l.id from Location l where l.synced = ?1", false);
        for (String id : l) {
            if(postLocation(id)){
                Location c = localFetchLocation(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }



    public List<Location> allLocations(){
        return Persistor.fetch("select l from Location l where l.description != ?1","main");
    }

    public void insert(Location location){
        Persistor.insert(location);
    }

    public Location find(String id){
        return (Location) Persistor.find(Location.class, id);
    }


}
