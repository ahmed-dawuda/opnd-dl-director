package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.NewStockItem;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TransferUtil {


    private String holderId = GlobalValues.getCurrentSubject().getEmail();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    protected class IdHolderId {
        private String id;
        private String holderId;

        public IdHolderId(Object[] o) {
            id = (String) o[0];
            holderId = (String) o[1];
        }

        public boolean isInList(List<IdHolderId> list) {

            for (IdHolderId idHolderId : list)
                if (id.equalsIgnoreCase(idHolderId.getId()) && holderId.equalsIgnoreCase(idHolderId.getHolderId()))
                    return true;

            return false;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHolderId() {
            return holderId;
        }

        public void setHolderId(String holderId) {
            this.holderId = holderId;
        }

        @Override
        public String toString() {
            return "[id="+id+", holderId="+holderId+"]";
        }
    }

    private List<IdHolderId> localFetchIds() {
        List<IdHolderId> ids = new ArrayList<>();
        List l = Persistor.fetch("select i.id, i.holderId from TransferItem i");

        for (Object o : l) {
            ids.add(new IdHolderId((Object[]) o));
        }

        return ids;
    }

    private TransferItem localFetchTransferItem(String id) {
        return (TransferItem) Persistor.find(TransferItem.class, id);
    }

    private List<IdHolderId> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "transferItems/idsOnly";
        List<IdHolderId> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
//            LogUtil.log(object.toString(2));
            for (int i = 0; i < idListJson.length(); i++) {
//                ids.add(new Gson().fromJson(idListJson.toString(), IdHolderId.class));
//                LogUtil.log(idListJson.getJSONObject(i).toString());
                ids.add(new IdHolderId(new Object[]{idListJson.getJSONObject(i).getString("id"), idListJson.getJSONObject(i).getString("holderId")}));
            }
//            LogUtil.log(object.getJSONArray("message").toString());

        }
//        LogUtil.log(object.toString(2));
//        LogUtil.log(ids.toString());
        return ids;
    }

    private TransferItem remoteFetchTransferItem(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "transferItems/" + id.getId() + "/" + id.getHolderId();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            TransferItem item = new TransferItem();
            item.setId(itemJson.getString("id"));
            item.setSynced(true);
            item.setQuantity(itemJson.getInt("quantity"));
            item.setProductId(itemJson.getString("productId"));
            item.setHolderId(itemJson.getString("holderId"));
//            item.setDate(new Date(itemJson.getLong("date")));
            item.setRequisitionId(itemJson.getString("requisitionId"));
//            Gson gson = new Gson();
//            return gson.fromJson(itemJson.toString(), InventoryItem.class);
            return item;
        }

        return null;
    }

    private boolean downloadTransferItem(IdHolderId id) throws UnirestException {
        TransferItem item = remoteFetchTransferItem(id);
//        LogUtil.log(item.toString());
//        return false;
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postTransferItem(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "transferItems";
        TransferItem item = localFetchTransferItem(id.getId());
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }



    public void sync() throws UnirestException {
        List<IdHolderId> localList = localFetchIds();
        List<IdHolderId> remoteList = remoteFetchIds();

        List<IdHolderId> localOnly = new ArrayList<>();
        List<IdHolderId> remoteOnly = new ArrayList<>();

        for (IdHolderId number : localList) {
            if (!number.isInList(remoteList))
                localOnly.add(number);
        }

        for (IdHolderId number : remoteList) {
            if (!number.isInList(localList))
                remoteOnly.add(number);
        }

        for (IdHolderId number : localOnly) {
            postTransferItem(number);
        }

        for (IdHolderId number : remoteOnly) {
            downloadTransferItem(number);
        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List l = Persistor.fetch("select i.id, i.holderId from TransferItem i where i.synced = ?1", false);
        for (Object id : l) {
            IdHolderId idH = new IdHolderId((Object[]) id);
            if(postTransferItem(idH)){
                TransferItem i = localFetchTransferItem(idH.getId());
                Persistor.begin();
                i.setSynced(true);
                Persistor.commit();
            }
        }
//        LogUtil.log(l.toString());
    }





    public static void insert(NewStockItem newStockItem){
        TransferItem transferItem = new TransferItem();
//        transferItem.setDate(new Date());
        transferItem.setQuantity(newStockItem.getQuantity());
//        transferItem.setDirection(0);
//        transferItem.setHolderId(342345);
        transferItem.setProductId(newStockItem.getProduct().getId());
        Persistor.insert(transferItem);
    }

}
