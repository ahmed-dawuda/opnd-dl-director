package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.NewStockItem;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StockUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

        for (String number : localList) {
            if (!remoteList.contains(number))
                localOnly.add(number);
        }

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }

        for (String number : localOnly) {
            postNewStockItem(number);
        }

        for (String number : remoteOnly) {
            downloadNewStockItem(number);
        }
    }

    @SuppressWarnings("unchecked")
    private void postUnsynced()throws UnirestException{
        List<String> l = Persistor.fetch("select s.id from StockItem s where s.synced = ?1", false);
        for (String id : l) {
            if(postNewStockItem(id)){
                NewStockItem n = localFetchStockItem(id);
                Persistor.begin();
                n.setSynced(true);
                Persistor.commit();
            }
        }
    }

    private boolean postNewStockItem(String id) throws UnirestException {
        String url = BASE_URL + "newStockItems";
        NewStockItem item = localFetchStockItem(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    private boolean downloadNewStockItem(String id) throws UnirestException {
        NewStockItem item = remoteFetchStockItem(id);

        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select s.id from NewStockItem s");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private NewStockItem localFetchStockItem(String id) {
        return (NewStockItem) Persistor.find(NewStockItem.class, id);
    }

    private NewStockItem remoteFetchStockItem(String id) throws UnirestException {
        String url = BASE_URL + "newStockItems/" + id + "/" + holderId;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), NewStockItem.class);
        }

        return null;
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "newStockItems/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }
}
