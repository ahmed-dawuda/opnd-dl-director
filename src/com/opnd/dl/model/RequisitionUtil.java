package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.TransferRequestWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RequisitionUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    protected class IdHolderId {
        private String id;
        private String holderId;

        public IdHolderId(Object[] o) {
            id = (String) o[0];
            holderId = (String) o[1];
        }

        public boolean isInList(List<IdHolderId> list) {

            for (IdHolderId idHolderId : list)
                if (id.equalsIgnoreCase(idHolderId.getId()) && holderId.equalsIgnoreCase(idHolderId.getHolderId()))
                    return true;

            return false;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHolderId() {
            return holderId;
        }

        public void setHolderId(String holderId) {
            this.holderId = holderId;
        }

        @Override
        public String toString() {
            return "id="+id+", holderId="+holderId;
        }
    }

    private List<IdHolderId> localFetchIds() {
        List<IdHolderId> ids = new ArrayList<>();
        List l = Persistor.fetch("select r.id, r.holderId from Requisition r");

        for (Object o : l) {
            ids.add(new IdHolderId((Object[]) o));
        }

        return ids;
    }

    private Requisition localFetchRequisition(String id) {
        return (Requisition) Persistor.find(Requisition.class, id);
    }

    private List<IdHolderId> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "requisitions/idsOnly";
        List<IdHolderId> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
//            LogUtil.log(object.toString(2));
            for (int i = 0; i < idListJson.length(); i++) {
//                LogUtil.log(idListJson.getJSONObject(i).toString());
                ids.add(new IdHolderId(new Object[]{idListJson.getJSONObject(i).getString("id"), idListJson.getJSONObject(i).getString("holderId")}));
            }

        }
        return ids;
    }

    private Requisition remoteFetchRequisition(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "requisitions/" + id.getId() + "/" + id.getHolderId();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Requisition item = new Requisition();
            item.setId(itemJson.getString("id"));
            item.setSynced(true);
            item.setStatus(Requisition.RequisitionStatus.valueOf(itemJson.getString("status")));
            item.setDate(new Date(itemJson.getLong("date")));
            item.setHolderId(itemJson.getString("holderId"));
             try {
                 if(itemJson.getInt("receivedOn") != 0)
                     item.setReceivedOn(new Date(itemJson.getLong("receivedOn")));
                 if(itemJson.getInt("approvedOn") != 0)
                     item.setApprovedOn(new Date(itemJson.getLong("approvedOn")));
                 if(itemJson.getInt("issuedOn")!= 0)
                     item.setIssuedOn(new Date(itemJson.getLong("issuedOn")));
             }catch (Exception ex){

             }
            item.setTargetId(itemJson.getString("targetId"));
//            Gson gson = new Gson();
//            return gson.fromJson(itemJson.toString(), InventoryItem.class);
            return item;
        }

        return null;
    }

    private boolean downloadRequisition(IdHolderId id) throws UnirestException {
        Requisition item = remoteFetchRequisition(id);
//        LogUtil.log(item.toString());
//        return false;
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postRequisition(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "requisitions";
        Requisition item = localFetchRequisition(id.getId());
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<IdHolderId> localList = localFetchIds();
        List<IdHolderId> remoteList = remoteFetchIds();

        List<IdHolderId> localOnly = new ArrayList<>();
        List<IdHolderId> remoteOnly = new ArrayList<>();

        for (IdHolderId number : localList) {
            if (!number.isInList(remoteList))
                localOnly.add(number);
        }

        for (IdHolderId number : remoteList) {
            if (!number.isInList(localList))
                remoteOnly.add(number);
        }

        for (IdHolderId number : localOnly) {
            postRequisition(number);
        }

        for (IdHolderId number : remoteOnly) {
            downloadRequisition(number);
        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List l = Persistor.fetch("select r.id, r.holderId from Requisition r where r.synced = ?1", false);
        for (Object id : l) {
            IdHolderId idH = new IdHolderId((Object[]) id);
            if(postRequisition(idH)){
                Requisition i = localFetchRequisition(idH.getId());
                Persistor.begin();
                i.setSynced(true);
                Persistor.commit();
            }
        }
//        LogUtil.log(l.toString());
    }


    public List<Requisition> listRequisitions() {
       return Persistor.fetch("select r from Requisition r where r.status = ?1", Requisition.RequisitionStatus.PENDING);
    }

    public List<TransferRequestWrapper> listWrappedRequisitions(){
        List<Requisition> items = listRequisitions();
        List<TransferRequestWrapper> transferRequestWrappers = new ArrayList<>();
        for(Requisition requisition : items){
            transferRequestWrappers.add(new TransferRequestWrapper(requisition));
        }
        return transferRequestWrappers;
    }

    public void approveRequisition(Subject target){
        Requisition requisition = GlobalValues.getRequisition();
        Persistor.begin();
        requisition.setStatus(Requisition.RequisitionStatus.APPROVED);
        requisition.setTargetId(target.getId());
        requisition.setUpdated(false);
        requisition.setApprovedOn(Date.from(Instant.now()));
        Persistor.commit();
    }


    private boolean putRequisition(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "requisitions/"+id.getId()+"/"+id.getHolderId();
        Requisition item = localFetchRequisition(id.getId());
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.put(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    @SuppressWarnings("unchecked")
    public void putUpdated() throws UnirestException{
        List l = Persistor.fetch("select r.id, r.holderId from Requisition r where r.updated = ?1", false);
        for (Object id : l) {
            IdHolderId idH = new IdHolderId((Object[]) id);
            if(putRequisition(idH)){
                Requisition i = localFetchRequisition(idH.getId());
                Persistor.begin();
                i.setUpdated(true);
                Persistor.commit();
            }
        }
//        LogUtil.log(l.toString());
    }

}
