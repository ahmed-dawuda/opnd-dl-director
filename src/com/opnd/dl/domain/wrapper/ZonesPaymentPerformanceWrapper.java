package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.Persistor;

import java.util.List;

public class ZonesPaymentPerformanceWrapper {
    private Integer rank;
    private String name;
    private String location;
    private Double sales;

    public ZonesPaymentPerformanceWrapper(Subject subject, int rank, int month, int year){
        this.rank = rank;
        this.name = subject.getFullname();
        this.location = ((Location) Persistor.find(Location.class, subject.getLocationId())).getDescription();
        String date = month < 10 ? year + "-0" + month + "-" :  year + "-" + month + "-";
        String query = "select sum(s.amount) from Payment s where FUNC('DATE', s.date, 'unixepoch') >= ?1 and FUNC('DATE', s.date, 'unixepoch') <= ?2 and s.holderId = ?3";
        List sales = Persistor.fetch(query, date + "1", date + "31", subject.getId());
        this.sales = sales.get(0) == null ? 0 : (Double) sales.get(0);
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getSales() {
        return sales;
    }

    public void setSales(Double sales) {
        this.sales = sales;
    }
}
