package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.*;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class InventoryWrapper {
    private String inventoryItemId;
    private double unitprice;
    private double quantity;
    private double amount;
    private Product product;
    private Category category;
    private Date date;
    private Location location;

    public InventoryWrapper(InventoryItem inventoryItem){
        this.inventoryItemId = inventoryItem.getId();
        this.quantity = inventoryItem.getQuantity();
        this.date = inventoryItem.getDate();
        this.product = (Product) Persistor.find(Product.class, inventoryItem.getProductId());
        this.category = (Category) Persistor.find(Category.class, product.getCategoryId());
        this.amount = this.product.getPrice() * this.quantity;
        this.unitprice = this.product.getPrice();
        Subject holder = (Subject) Persistor.find(Subject.class, inventoryItem.getHolderId());
//        Location loc = (Location) Persistor.fetchSingle("select l from Location l where l.deleted = ?1 and l.id = ?2",false,holder.getLocationId());

        this.location = (Location) Persistor.find(Location.class, holder.getLocationId());

    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(String inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
