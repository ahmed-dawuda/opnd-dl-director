package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class TransferRequestWrapper {
    private Requisition requisition;
    private Date date;
    private Subject requestedBy;
    private Requisition.RequisitionStatus status;
    private Location location;
//    private Date responseDate;

    public TransferRequestWrapper(Requisition requisition){
        this.requisition = requisition;
        this.date = requisition.getDate();
        this.requestedBy = (Subject) Persistor.find(Subject.class,requisition.getHolderId());
        this.status = requisition.getStatus();
        location = (Location) Persistor.find(Location.class, requestedBy.getLocationId());
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public TransferRequestWrapper(){ super(); }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Subject getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(Subject requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Requisition.RequisitionStatus getStatus() {
        return status;
    }

    public void setStatus(Requisition.RequisitionStatus status) {
        this.status = status;
    }

//    public Date getResponseDate() {
//        return responseDate;
//    }
//
//    public void setResponseDate(Date responseDate) {
//        this.responseDate = responseDate;
//    }
}
