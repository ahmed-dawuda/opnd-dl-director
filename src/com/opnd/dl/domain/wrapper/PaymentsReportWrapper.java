package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Location;
import com.opnd.dl.domain.Payment;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class PaymentsReportWrapper {
    private Date date;
    private Location location;
    private Subject paidBy;
    private Double amount;
    private Requisition.RequisitionStatus status;

    public PaymentsReportWrapper(Payment payment){
        date = payment.getDate();

        paidBy = (Subject) Persistor.find(Subject.class, payment.getHolderID());
//        LogUtil.log(paidBy.toString());
        amount = payment.getAmount();
        LogUtil.log(paidBy.getLocationId()+"");
        location = (Location) Persistor.find(Location.class, paidBy.getLocationId());
    }

    public Location getLocation() {
        return location;
    }


    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Subject getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(Subject paidBy) {
        this.paidBy = paidBy;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Requisition.RequisitionStatus getStatus() {
        return status;
    }

    public void setStatus(Requisition.RequisitionStatus status) {
        this.status = status;
    }
}
