package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.Product;
import com.opnd.dl.mStuff.Persistor;

public class ProductManagementWrapper {
    private String description;
    private Category category;
    private Double price;
    private String id;
    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductManagementWrapper(Product product){
        this.id = product.getId();
        this.category = (Category) Persistor.find(Category.class, product.getCategoryId());
        this.price = product.getPrice();
        this.description = product.getDescription();
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
