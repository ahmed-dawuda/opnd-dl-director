package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Payment;
import com.opnd.dl.domain.SaleItem;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import javafx.scene.control.TableView;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class CashTrackWrapper {
    private double amount;
    private Date date;
    private Subject person;

    public CashTrackWrapper(Subject subject){
        List<SaleItem> sales = Persistor.fetch("select s from SaleItem s where s.holderId = ?1", subject.getId());
        List<Payment> payments = Persistor.fetch("select p from Payment p where p.holderId = ?1", subject.getId());
        double totalSales = 0;
        double totalPayments = 0;
        for (SaleItem sale : sales) {
            totalSales += sale.getAmount();
        }
        for (Payment payment : payments) {
            totalPayments += payment.getAmount();
        }
        amount = totalSales - totalPayments;
        person = subject;
        date = Date.from(Instant.now());
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Subject getPerson() {
        return person;
    }

    public void setPerson(Subject person) {
        this.person = person;
    }
}
