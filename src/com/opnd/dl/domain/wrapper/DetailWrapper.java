package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.mStuff.Persistor;

public class DetailWrapper {
    private String transferItemId;
    private Product product;
    private Double quantity;
    private Double amount;

    public DetailWrapper(TransferItem transferItem){
        this.transferItemId = transferItem.getId();
        this.product = (Product) Persistor.find(Product.class, transferItem.getProductId());
        this.quantity = transferItem.getQuantity();
        this.amount = this.quantity * this.product.getPrice();
    }

    public String getTransferItemId() {
        return transferItemId;
    }

    public void setTransferItemId(String transferItemId) {
        this.transferItemId = transferItemId;
    }

    public DetailWrapper(){}

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
