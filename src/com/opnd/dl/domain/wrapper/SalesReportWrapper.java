package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.*;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class SalesReportWrapper {
    private Date date;
    private Date time;
    private Location location;
    private Product product;
    private Subject salesperson;
    private Double quantity;
    private Double price;
    private Double amount;

    public SalesReportWrapper(SaleItem saleItem){
        this.date = saleItem.getDate();
        this.product = (Product) Persistor.find(Product.class, saleItem.getProductId());
        this.quantity = saleItem.getQuantity();
        this.amount = saleItem.getAmount();
        this.price = this.product.getPrice();
        this.time = saleItem.getDate();
        Subject holder = (Subject) Persistor.find(Subject.class, saleItem.getHolderId());
        this.location = (Location) Persistor.find(Location.class, holder.getLocationId());
        this.salesperson = (Subject) Persistor.find(Subject.class,saleItem.getHolderId());
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Subject getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(Subject salesperson) {
        this.salesperson = salesperson;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
