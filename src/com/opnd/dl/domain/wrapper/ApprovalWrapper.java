package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.*;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;

import java.util.List;

public class ApprovalWrapper {
    private Subject person;
    private Role role;
    private Location location;
    private boolean hasEnough = true;

    public ApprovalWrapper(Subject  person){
        this.person = person;
        role = (Role) Persistor.find(Role.class, person.getRoleId());
        location = (Location) Persistor.find(Location.class, person.getLocationId());
        Requisition requisition = GlobalValues.getRequisition();
        List<TransferItem> transferItems = Persistor.fetch("Select t from TransferItem t where t.requisitionId = ?1", requisition.getId());

        for (TransferItem transferItem : transferItems) {
            List<InventoryItem> inventoryItem =  Persistor.fetch("Select i from InventoryItem i where i.productId = ?1 and i.holderId = ?2",transferItem.getProductId(),person.getId());
            if(inventoryItem.size() > 0){

                if(inventoryItem.get(0).getQuantity() > transferItem.getQuantity()){
                    hasEnough &= true;
                }else{
                    hasEnough &= false;
                }
            }else{
                hasEnough = false;
            }
        }
    }

    public Subject getPerson() {
        return person;
    }

    public void setPerson(Subject person) {
        this.person = person;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isHasEnough() {
        return hasEnough;
    }

    public void setHasEnough(boolean hasEnough) {
        this.hasEnough = hasEnough;
    }
}
