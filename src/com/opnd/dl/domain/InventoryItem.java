package com.opnd.dl.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class InventoryItem {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
	private String productId;
	private String holderId;
	private double quantity;
	private Date date;
    private boolean deleted;
    private boolean synced = false;
    private boolean updated = true;

    @Override
    public String toString() {
        return "InventoryItem{" +
                "id=" + id +
                ", productId=" + productId +
                ", holderId=" + holderId +
                ", quantity=" + quantity +
                ", date=" + date +
                ", deleted=" + deleted +
                ", synced=" + synced +
                ", updated=" + updated +
                '}';
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}