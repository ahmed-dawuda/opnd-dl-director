package com.opnd.dl.mStuff;

import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;

import java.util.List;

public class GlobalValues {

    private static final String AppTitle = "Desert Lion Group";
//    private static final String serverUrl = "http://api.exceptionalladies.com";
    private static final String serverUrl = "http://localhost/api.exceptionalladies.com/index.php";

    private static final String baseUrl = "http://microlifesoft.com/api/";

    private static volatile boolean running = false;

    public static boolean isRunning() {
        return running;
    }

    public static void setRunning(boolean running) {
        GlobalValues.running = running;
    }

    private static Requisition requisition;

    public static Requisition getRequisition() {
        return requisition;
    }

    public static void setRequisition(Requisition requisition) {
        GlobalValues.requisition = requisition;
    }


    public static String getBaseUrl() {
        return baseUrl;
    }

    private static Subject currentSubject = null;
    private static Object sharedObject = null;

    public static Object getSharedObject() {
        return sharedObject;
    }

    public static void setSharedObject(Object sharedObject) {
        GlobalValues.sharedObject = sharedObject;
    }

    public static Subject getCurrentSubject() {
        return currentSubject;
    }

    public static void setCurrentSubject(Subject currentSubject) {
        GlobalValues.currentSubject = currentSubject;
    }

    public static String getServerUrl() {
        return serverUrl;
    }

    public static String getAppTitle() {
        return AppTitle;
    }

    public static String generateId(){
        return currentSubject.getId()+""+ System.currentTimeMillis();
    }

    public static String generateId(String table){
        String query = "Select o from "+table+" o";
        List<Subject> subjects = Persistor.fetch(query);
        int n = subjects.size() + 1;
        return n+"";//+ ((int) Math.random()*100);
    }
}
