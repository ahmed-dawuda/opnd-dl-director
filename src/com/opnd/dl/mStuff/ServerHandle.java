package com.opnd.dl.mStuff;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;
import org.apache.http.NameValuePair;

import java.util.*;

public class ServerHandle {

    private static Queue<ServerTask> serverTaskQueue = new ArrayDeque<>();
    private static boolean processing = false;

    public static void addTaskToQueue(ServerTask task){
        serverTaskQueue.add(task);
        if (!processing) processQueue();
    }

    public static void executeTask(ServerTask task) {
        HttpRequestWithBody request = Unirest.post(GlobalValues.getServerUrl());
        MultipartBody body = request.field("transaction", task.getTransaction());
        for (NameValuePair nameValuePair : task.getFields()) {
            body = body.field(nameValuePair.getName(), nameValuePair.getValue());
        }
        body.asJsonAsync(task.responseHandler);
    }

    private static void processQueue() {
        processing = true;
        while (checkConnection() && !serverTaskQueue.isEmpty())
            processTask(serverTaskQueue.remove());

        processing = false;
    }

    private static void processTask(ServerTask task) {
        HttpRequestWithBody request = Unirest.post(GlobalValues.getServerUrl());
        MultipartBody body = request.field("transaction", task.getTransaction());
        for (NameValuePair nameValuePair : task.getFields()) {
            body = body.field(nameValuePair.getName(), nameValuePair.getValue());
        }
        body.asJsonAsync(task.responseHandler);
    }

    private static boolean checkConnection() {
        try {
            Unirest.post(GlobalValues.getServerUrl()).asString();
            LogUtil.log("Server connection established");
        } catch (UnirestException e) {
//            e.printStackTrace();
            LogUtil.log("Could not connect to the server");
            return false;
        }

        return true;
    }

    public enum ServerTransaction {LOGIN, MESSAGES, SALES, PAYMENTS};

    public static class ServerTask {
        private ServerTransaction transaction;
        private List<NameValuePair> fields;
        private ServerResponseHandler responseHandler;

        public ServerTask() {
            fields = new ArrayList<>();
        }

        public void addParameter(NameValuePair param){
            fields.add(param);
        }

        public ServerTask(ServerTransaction transaction, NameValuePair... params) {
            this.transaction = transaction;
            fields = new ArrayList<>();
            fields.addAll(Arrays.asList(params));
        }

        public void setResponseHandler(ServerResponseHandler handler) {
            this.responseHandler = handler;
            responseHandler.setTask(this);
        }

        public void setTransaction(ServerTransaction transaction) {
            this.transaction = transaction;
        }

        public ServerTransaction getTransaction() {
            return transaction;
        }

        public List<NameValuePair> getFields() {
            return fields;
        }
    }

    public static abstract class ServerResponseHandler implements Callback<JsonNode> {
        private ServerTask task;

        public void setTask(ServerTask task) {
            this.task = task;
        }
    }
}
