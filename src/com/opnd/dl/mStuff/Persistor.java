package com.opnd.dl.mStuff;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.File;
import java.util.List;

public class Persistor {

    private static EntityManager em;

    private static EntityManagerFactory factory;

    public static void init(String persistence_name){
        String dbLocation = "database/sqlite";
        File dbFolder = new File(dbLocation);
        boolean mkdirs = true;

        if (!dbFolder.exists()) {
            mkdirs = dbFolder.mkdirs();
        }

        if (!mkdirs) {
            System.out.println("Could not create database directory.\n" +
                    "The program will quit now");
            System.exit(-1);
//            ViewHelper viewHelper = new ViewHelper();
//            viewHelper.msgBox(Alert.AlertType.ERROR,
//                    "Database Error",
//                    "IO Error",
//                    "Could not create the directory in which the database file will be saved").show();
        }

        factory = Persistence.createEntityManagerFactory(persistence_name);
        em = factory.createEntityManager();
    }

    public static EntityManager getEm() {
        return em;
    }

    public static void begin(){
        em.getTransaction().begin();
    }

    public static void commit(){
        em.getTransaction().commit();
    }

    public static void insert(Object o){
        begin();
        em.persist(o);
        commit();
    }

    public static void delete(Object o){
        begin();
        em.remove(o);
        commit();
    }

    public static Object find(Class t, Object id){
        return em.find(t, id);
    }

    public static List fetchAll(String table) {
        Query q = em.createQuery("select m from " + table + " m");
        return q.getResultList();
    }

    public static List fetch(String query, Object... params){
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        return q.getResultList();
    }

    public static Object fetchSingle(String query, Object... params) {
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        return q.getSingleResult();
    }

    public static int update(String query, Object... params){
        begin();
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        int i = q.executeUpdate();
        commit();

        return i;
    }

    public static void close(){
        em.close();
        factory.close();
    }
}
