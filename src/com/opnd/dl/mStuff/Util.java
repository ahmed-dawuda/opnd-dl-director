package com.opnd.dl.mStuff;

import com.opnd.dl.domain.Subject;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class Util {

    public LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public LocalTime toLoccalTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    public Date toUtilDate(LocalDate date) {
        try {
            return DateFormat.getInstance().parse(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Date.from(Instant.now());
    }

    public void createAdminUser() {
        List<Subject> users = Persistor.fetch("select a from Subject a");
//        System.out.println(users.size());

        if (users.size() < 1) {
            Subject admin = new Subject();
            admin.setFullname("Administrator Admin");
            admin.setFirstname("Admin");
            admin.setPassword("opnd");
            admin.setEmail("admin");
            Persistor.insert(admin);
        }
    }
}
